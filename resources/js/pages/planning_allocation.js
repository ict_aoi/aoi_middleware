$(function()
{
    var planningAllocationTable = $('#planningAllocationTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:10,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/erp/planning-allocation/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = planningAllocationTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'c_order_id', name: 'c_order_id',searchable:true,visible:false,orderable:true},
            {data: 'supplier_code', name: 'supplier_code',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'documentno', name: 'documentno',searchable:true,visible:false,orderable:true},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#planningAllocationTable').dataTable().api();
    $("#planningAllocationTable.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
});

function insertAllocation(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        }
    })
    .done(function () {
        $.unblockUI();
        $("#alert_success").trigger("click", 'Insert to allocation erp successfully.');
        $('#planningAllocationTable').DataTable().ajax.reload();
    });
}