@extends('layouts.app',['active' => 'user'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li >Account Management</li>
            <li class="active">User</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="{{ route('user.create')}}" class="btn btn-default" ><i class="icon-plus2 position-left"></i>Create</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-basic table-striped table-hover" id="userTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id</th>
                        <th>Perusahaan</th>
                        <th>Pabrik</th>
                        <th>Departemen</th>
                        <th>Nik</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
{!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-modal')

@endsection

@section('page-js')
<script src="{{ mix('js/user.js') }}"></script>
@endsection
