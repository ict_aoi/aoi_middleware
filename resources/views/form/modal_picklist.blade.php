<div id="{{ $name }}Modal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">{{ $title }}</h5>
			</div>

			<div class="modal-body">
				<fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="text" id="{{ $name }}Search"  class="form-control" placeholder="Masukan kata kunci..." autofocus="true">
								<span class="input-group-btn">
									<button class="btn bg-teal legitRipple" type="button" id="{{ $name }}ButtonSrc"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
								</span>
							</div>
							<span class="help-block">{{ $placeholder }}</span>
								
						</div>
					</div>
				</fieldset>
				<br/>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/images/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="{{ $name }}Table"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
