@extends('layouts.app',['active' => 'planning_allocation'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Planning Allocation</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li >Erp</li>
            <li class="active">Planning Allocation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-basic table-striped table-hover" id="planningAllocationTable">
                <thead>
                    <tr>
						<th>No</th>
					    <th>id</th>
					    <th>Supplier Code</th>
                        <th>Supplier Name</th>
                        <th>Po Supplier</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/planning_allocation.js') }}"></script>
@endsection

