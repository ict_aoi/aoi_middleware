<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/order-receipt', 'Integration\IntegrationController@OrderReceipt');
Route::get('/reverse-receipt', 'Integration\IntegrationController@ReverseReceipt');

Route::get('/daily-attedance-sync', 'Integration\AttendanceController@daily_sync');

// fgms header so
Route::get('/plan-load-sync', 'Integration\HeaderSoController@planLoadInsert');

// fgms stock balance
Route::get('/stock-balance-sync', 'Integration\FinishGoodsController@StockBalanceInsert');

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');

Route::prefix('erp')->group(function () 
{
    Route::prefix('planning-allocation')->group(function () 
    {
        Route::get('', 'ERP\PlanningAllocationController@index')->name('planningAllocation.index');
        Route::get('data', 'ERP\PlanningAllocationController@data')->name('planningAllocation.data');
        Route::post('allocation/{id}', 'ERP\PlanningAllocationController@allocation')->name('planningAllocation.allocation');
    });
});

Route::prefix('account-management')->group(function () 
{
    Route::prefix('permission')->middleware(['permission:menu-permission'])->group(function(){
        Route::get('', 'PermissionController@index')->name('permission.index');
        Route::get('create', 'PermissionController@create')->name('permission.create');
        Route::get('data', 'PermissionController@data')->name('permission.data');
        Route::post('store', 'PermissionController@store')->name('permission.store');
        Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
        Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
        Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
    });

    Route::prefix('role')->middleware(['permission:menu-role'])->group(function(){
        Route::get('', 'RoleController@index')->name('role.index');
        Route::get('create', 'RoleController@create')->name('role.create');
        Route::get('data', 'RoleController@data')->name('role.data');
        Route::get('edit/{id}', 'RoleController@edit')->name('role.edit');
        Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
        Route::post('store', 'RoleController@store')->name('role.store');
        Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
        Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
        Route::post('update/{id}', 'RoleController@update')->name('role.update');
        Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
    });

    Route::prefix('user')->middleware(['permission:menu-user'])->group(function(){
        Route::get('', 'UserController@index')->name('user.index');
        Route::get('create', 'UserController@create')->name('user.create');
        Route::get('data', 'UserController@data')->name('user.data');
        Route::get('get-absence', 'UserController@getAbsence')->name('user.getAbsence');
        Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
        Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
        Route::post('store', 'UserController@store')->name('user.store');
        Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
        Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
        Route::post('update/{id}', 'UserController@update')->name('user.update');
        Route::put('reset-password/{id}', 'UserController@resetPassword')->name('user.resetPassword');
        Route::put('delete/{id}', 'UserController@destroy')->name('user.destroy');
    });
});

Route::prefix('report')->group(function(){
    Route::get('/packingin', 'Report\ReportController@packingIn');
    Route::get('/packingout', 'Report\ReportController@packingout');
    Route::get('/sewingin', 'Report\ReportController@sewingin');
    Route::get('/sewingout', 'Report\ReportController@sewingout');


    Route::get('/ppc/checkPO', 'Report\ProdController@cronData');
    Route::get('/ppc/cekPPC', 'Report\ProdController@cekPPC');
    Route::get('/ppc/clean', 'Report\ProdController@clean');
});