<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('/packing/customer-information', 'API\PackingController@customerDetailInformation');

Route::group(['middleware' => 'auth:api'], function(){
	Route::get('/user-information', 'API\UserController@details');
});