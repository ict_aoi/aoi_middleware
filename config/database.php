<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'finger_aoi_1' => [
            'driver' => 'mysql',
            'host' => '192.168.15.25',
            'port' => '3366',
            'database' => 'ftm',
            'username' => 'profinger',
            'password' => '',
            'unix_socket' => env('DB_SOCKET', ''),
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'finger_aoi_2' => [
            'driver' => 'mysql',
            'host' => '192.168.15.55',
            'port' => '3366',
            'database' => 'ftm',
            'username' => 'profinger',
            'password' => '',
            'unix_socket' => env('DB_SOCKET', ''),
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'finger_bbis' => [
            'driver' => 'mysql',
            'host' => '192.168.61.16',
            'port' => '3366',
            'database' => 'ftm',
            'username' => 'profinger',
            'password' => '',
            'unix_socket' => env('DB_SOCKET', ''),
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'erp_live' => [
            'driver'    => 'pgsql',
            'host'      => env('ERP_HOST_REMOTE', '192.168.51.123'),
            'port'      => env('ERP_PORT_REMOTE', '5432'),
            'database'  => 'aimsdbc',
            'username'  => 'adempiere',
            'password'  => 'Becarefulwithme',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'adempiere',
            'sslmode'   => 'prefer',
        ],

        // fgms aoi live
        'fgms_aoi'      => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.19',
            'port'      => '5432',
            'database'  => 'finish_goods',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        // fgms aoi dev
        'fgms_dev_aoi'  => [
            'driver'    => 'pgsql',
            'host'      => '192.168.61.80',
            'port'      => '5432',
            'database'  => 'fg_060422',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        // cdms dev
        'cdms_dev' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.15',
            'port'      => '5432',
            'database'  => 'cdms_dev',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        // cdms live
        'cdms_live' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.53',
            'port'      => '5432',
            'database'  => 'cdms',
            'username'  => 'postgres',
            'password'  => 'Becarefulwithme',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        // sds live
        'sds_live' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.32',
            'port'      => '5432',
            'database'  => 'smart_dist',
            'username'  => 'smartdist',
            'password'  => 'Pass@1234',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        // line live
        'line_live' => [
            'driver' => 'pgsql',
            'host' => '192.168.61.112',
            'port' => '5432',
            'database' => 'line_system',
            'username' => 'postgres',
            'password' => 'Becarefulwithme',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'wms_live' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.22',
            'port'      => '5432',
            'database'  => 'wmsacc',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'absensi' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.15.56',
            'port'      => '5432',
            'database'  => 'absensi_aoi',
            //'database' => 'absensi_restore',
            'username'  => 'absensi',
            'password'  => 'Absensi12345',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'absensi_bbis' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.15.56',
            'port'      => '5432',
            //'database' => 'absensi_aoi',
            'database'  => 'absensi_bbi_restore',
            'username'  => 'absensi',
            'password'  => 'Absensi12345',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'web_po' => [
            'driver' => 'pgsql',
            'host' => env('WEBPO_HOST_REMOTE', '192.168.51.52'),
            'port' => env('WEBPO_PORT_REMOTE', '5432'),
            'database' => 'import',
            'username' => 'webpo',
            'password' => 'Becarefulwithme',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'wms_dev' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.15',
            'port'      => '5432',
            'database'  => 'wmsacc_050721',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'dashboard_wms' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.16',
            'port'      => '5432',
            'database'  => 'dashboard_wms',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'replika_wms' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.61.111',
            // 'host'      => '192.168.51.22',
            'port'      => '5432',
            'database'  => 'wmsacc',
            'username'  => 'dev',
            'password'  => 'Pass@123',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'dwh_aoi' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.76',
            'port'      => '5432',
            'database'  => 'dwh_aoi',
            'username'  => 'whs',
            'password'  => 'aoiadmin',
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'public',
            'sslmode'   => 'prefer',
        ],

        'bima_dev' => [
            'driver'    => 'pgsql',
            'host'      => '192.168.51.93',// 192.168.51.25
            'port'      => '5432', //5432
            'database'  => 'bimadbl', //aims13
            'username'  => 'adempiere', //adempiere
            'password'  => 'Becarefulwithme', //Becarefulwithme
            'charset'   => 'utf8',
            'prefix'    => '',
            'schema'    => 'adempiere',
            'sslmode'   => 'prefer',
        ],


        'line_aoi' => [
            'driver' => 'pgsql',
            'host' => '192.168.51.8',
            'port' => '5432',
            'database' => 'line_system',
            'username' => 'postgres',
            'password' => 'Becarefulwithme',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
