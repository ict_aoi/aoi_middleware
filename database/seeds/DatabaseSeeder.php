<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = [
            'permissions',
            'roles',
            'users',
        ];

    	Model::unguard();
        
        foreach ($tables as $key => $table) {
            DB::statement("TRUNCATE " . $table . " CASCADE ;");
        }

        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        
        //DB::statement("SELECT SETVAL('users_id_seq'::regclass, 1)"); 
        //static::ResetAutoincrement('users');
        Model::reguard();
    }
}
