<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user = new User();
        $user->name = 'Admin ICT AOI';
        $user->nik = '11111111';
        $user->email = 'admin_ict@aoi.co.id';
        $user->password =  bcrypt('password1');

        if($user->save())
            $user->attachRoles($roles);   
    }
}
