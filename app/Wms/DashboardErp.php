<?php

namespace App\Wms;

use DB;
use Config;
use StdClass;

use Illuminate\Database\Eloquent\Model;

class DashboardErp extends Model
{
    protected $connection   = 'dashboard_wms';
    // protected $connection   = 'erp';
    //protected $guarded      = ['id'];
    protected $fillable     = ['id',
    'type_stock',
    'promise_date',
    'statistical_date',
    'booking_number',
    'created_at',
    'user_pic',
    'update_user',
    'lc_date',
    'season',
    'document_no',
    'supplier_code',
    'supplier_name',
    'po_buyer',
    'old_po_buyer',
    'item_code_source',
    'item_code',
    'item_desc',
    'category',
    'warehouse_id',
    'warehouse_name',
    'uom',
    'qty_allocation',
    'qty_outstanding',
    'qty_allocated',
    'qty_in_house',
    'qty_on_ship',
    'is_fabric',
    'is_additional',
    'remark_additional',
    'status_po_buyer',
    'cancel_date',
    'remark_update',
    'is_allocation_purchase',
    'item_id_source',
    'c_order_id',
    'eta_date',
    'etd_date',
    'eta_actual',
    'etd_actual',
    'receive_date',
    'no_invoice',
    'mrd',
    'dd_pi',
    'is_closed',
    'eta_delay',
    'eta_max',
    'item_id_book',
    'status',
    'brand',
    '_style',
    'remark_delay_new',
    'supplier_code_bom',
    'supplier_name_bom',
    'qtyordered_garment'];
    protected $table        = 'rma_dashboard_monitoring';
}

