<?php

namespace App\Wms;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AutoAllocation extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $connection   = 'wms_dev';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','locked_date','lc_date','generate_form_booking','promise_date'];
    protected $fillable     = ['lc_date'
        ,'c_order_id'
        ,'created_at'
        ,'updated_at'
        ,'season'
        ,'document_no'
        ,'warehouse_name'
        ,'promise_date'
        ,'user_id'
        ,'update_user_id'
        ,'c_bpartner_id'
        ,'supplier_name'
        ,'po_buyer'
        ,'old_po_buyer'
        ,'is_reroute'
        ,'item_id_book'
        ,'item_code'
        ,'item_id_source'
        ,'item_code_source'
        ,'warehouse_id'
        ,'qty_allocation'
        ,'is_fabric'
        ,'is_already_generate_form_booking'
        ,'generate_form_booking'
        ,'is_inserted_to_allocation_item'
        ,'erp_allocation_id'
        ,'qty_outstanding'
        ,'uom'
        ,'qty_adjustment'
        ,'qty_allocated'
        ,'item_desc'
        ,'category'
        ,'type_stock'
        ,'type_stock_erp_code'
        ,'deleted_at'
        ,'remark'
        ,'document_allocation_number'
        ,'is_upload_manual'
        ,'is_allocation_purchase'
        ,'is_reduce'
        ,'qc_note'
        ,'qty_reject'
        ,'is_additional'
        ,'remark_additional'
        ,'article_no'
        ,'item_code_book'
        ,'status_po_buyer'
        ,'remark_update'
        ,'po_buyer_source'
        ,'is_from_menu_allocation_buyer'
        ,'is_ordering_for_japan_china'
        ,'locked_date'
        ,'cancel_date'
        ,'is_balance_marker'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function lastUpdateUser()
    {
        return $this->belongsTo('App\Models\User','update_user_id');
    }

    public function getTotalQtyOut($id)
    {
        $total_qty = MaterialPreparation::where('auto_allocation_id',$id)
        ->whereIn('last_status_movement',[
            'out',
            'reject',
            'out-subcont',
        ])
        ->sum(db::raw("(qty_conversion + COALESCE(adjustment,0) + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));

        return $total_qty;
    }

    public function getTotalQtyIn($id)
    {
        $total_qty = MaterialPreparation::where('auto_allocation_id',$id)
        ->whereNotIn('last_status_movement',[
            'out',
            'reject',
            'out-subcont',
        ])
        ->sum('qty_conversion');

        return $total_qty;
    }

    public function getTotalQtyPrepare($id)
    {
        $total_qty = MaterialPreparation::where('auto_allocation_id',$id)
        ->sum('qty_conversion');

        return $total_qty;
    }
}
