<?php

namespace App\Wms;


use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialStock extends Model
{
    use Uuids;
    protected $connection   = 'replika_wms';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'
    ,'updated_at'
    ,'deleted_at'
    ,'last_planning_date'
    ,'last_date_used'
    ,'inspect_lab_date'
    ,'insert_to_locator_reject_erp_date_from_rma'
    ,'insert_to_locator_reject_erp_date_from_inspect'
    ,'insert_to_locator_reject_erp_date_from_lot'
    ,'insert_to_locator_reject_erp_date_from_short_roll'
    ,'reject_by_lot_date'
    ,'reject_by_rma_date'
    ,'movement_to_locator_reject_date_from_rma'
    ,'movement_to_locator_reject_date_from_inspect'
    ,'movement_to_locator_reject_date_from_lot'
    ,'movement_to_locator_reject_date_from_short_roll'
    ,'sto_date'];

    protected $fillable     = ['locator_id'
    ,'auto_allocation_id'
    ,'is_stock_mm'
    ,'no_packing_list'
    ,'upc_item'
    ,'po_detail_id'
    ,'update_user_id'
    ,'available_qty'
    ,'barcode_supplier'
    ,'document_no'
    ,'item_id'
    ,'season'
    ,'begin_width'
    ,'middle_width'
    ,'end_width'
    ,'inspect_lab_date'
    ,'inspect_lab_remark'
    ,'material_roll_handover_fabric_id'
    ,'monitoring_receiving_fabric_id'
    ,'actual_length'
    ,'different_yard'
    ,'remark'
    ,'is_roll_cancel'
    ,'qty_order'
    ,'qty_arrival'
    ,'sto_remark'
    ,'sto_date'
    ,'is_sto'
    ,'is_short_roll'
    ,'sto_user_id'
    ,'is_stock_on_the_fly'
    ,'item_code'
    ,'item_desc'
    ,'created_at'
    ,'updated_at'
    ,'reserved_qty'
    ,'is_general_item'
    ,'approval_user_id'
    ,'approval_date'
    ,'actual_width'
    ,'load_actual'
    ,'last_date_used'
    ,'last_status'
    ,'last_user_used_id'
    ,'color'
    ,'is_closing_balance'
    ,'uom'
    ,'uom_source'
    ,'category'
    ,'batch_number'
    ,'nomor_roll'
    ,'po_buyer'
    ,'source'
    ,'c_bpartner_id'
    ,'supplier_name'
    ,'c_order_id'
    ,'last_planning_date'
    ,'last_user_planning_id'
    ,'is_used_to_relax'
    ,'no_invoice'
    ,'summary_stock_fabric_id'
    ,'sequence'
    ,'referral_code'
    ,'is_integration'
    ,'integration_date'
    ,'is_complete_erp'
    ,'supplier_code'
    ,'is_from_handover'
    ,'type_stock_erp_code'
    ,'type_stock'
    ,'mapping_stock_id'
    ,'is_printed'
    ,'ip_address'
    ,'is_stock_already_created'
    ,'stock'
    ,'warehouse_id'
    ,'type_po'
    ,'qty_carton'
    ,'is_active'
    ,'is_reject'
    ,'reject_by_lot_date'
    ,'is_reject_by_lot'
    ,'is_reject_by_rma'
    ,'qty_reject_by_inspect'
    ,'qty_reject_non_by_inspect'
    ,'qty_reject_by_short_roll'
    ,'movement_to_locator_reject_date_from_rma'
    ,'movement_to_locator_reject_date_from_inspect'
    ,'movement_to_locator_reject_date_from_lot'
    ,'movement_to_locator_reject_date_from_short_roll'
    ,'reject_by_rma_date'
    ,'inspect_lot_result'
    ,'is_master_roll'
    ,'insert_to_locator_reject_erp_date_from_rma'
    ,'insert_to_locator_reject_erp_date_from_inspect'
    ,'insert_to_locator_reject_erp_date_from_lot'
    ,'insert_to_locator_reject_erp_date_from_short_roll'
    ,'qc_result'
    ,'user_id'
    ,'deleted_at'
    ,'is_allocated'
    ,'is_material_others'
    ,'summary_handover_material_id'
    ,'is_running_stock'
    ,'material_stock_per_lot_id'
    ,'jenis_po'
    ,'invoice_confirm_id'];

}
