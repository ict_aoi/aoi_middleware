<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\WMS\InsertMrp::class,
        Commands\WMS\InsertOutstandingPoSupplier::class,
        Commands\ERP\InsertOutstandingPlanning::class,
        Commands\AttendanceSchedule::class,
        Commands\AttendanceScheduleAoi2::class,
        Commands\AttendanceScheduleBBIS::class,
        Commands\DailyPlanLoad::class,
        Commands\DailyStockBalance::class,
        Commands\DailySyncDataCutting::class,
        Commands\DailySyncDataCuttingLive::class,
        Commands\DailySyncCancel::class,
        Commands\OutputSyncDistribusi::class,
        Commands\OutputSyncDistribusiV2::class,
        Commands\SycnComponentReady::class,
        Commands\WMS\DailyUpdateDashboardMonitoringMaterialACC::class,
        Commands\WMS\dailyUpdateDashboadMonitoringMaterial::class,
        Commands\DailyOrderReceipt::class,
        Commands\DailyOrderReverse::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
