<?php

namespace App\Console\Commands;
use App\Http\Controllers\Integration\ComponentReadyController;

use App\Models\Scheduler;
use Carbon\Carbon;

use Illuminate\Console\Command;

class SycnComponentReady extends Command
{
    protected $signature = 'sycnComponentReady:insert';
    protected $description = 'Insert Component Ready';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_COMPONENT_READY')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC COMPONENT READY AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            ComponentReadyController::outputSync();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC COMPONENT READY AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_COMPONENT_READY')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_COMPONENT_READY',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC COMPONENT READY JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ComponentReadyController::outputSync();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC COMPONENT READY AT '.carbon::now());
            }else{
                $this->info('SYNC COMPONENT READY SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
