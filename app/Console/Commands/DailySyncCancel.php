<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Integration\FinishGoodsController;

use App\Models\Scheduler;
use Carbon\Carbon;


class DailySyncCancel extends Command
{
    protected $signature = 'dailySyncCancel:update';
    protected $description = 'Update Daily Sync Cancel';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_DAILY_SYNC_CANCEL')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC DAILY SYNC CANCEL AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            FinishGoodsController::sync_cancel();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY SYNC CANCEL AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_DAILY_SYNC_CANCEL')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_DAILY_SYNC_CANCEL',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC DAILY SYNC CANCEL JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                FinishGoodsController::sync_cancel();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC DAILY SYNC CANCEL AT '.carbon::now());
            }else{
                $this->info('SYNC DAILY SYNC CANCEL SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
