<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Integration\HeaderSoController;

use App\Models\Scheduler;
use Carbon\Carbon;


class DailyPlanLoad extends Command
{
    protected $signature = 'dailyPlanLoad:insert';
    protected $description = 'Insert Daily Plan Load';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_DAILY_PLAN_LOAD')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC DAILY PLAN LOAD AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            HeaderSoController::planLoadInsert();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY PLAN LOAD AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_DAILY_PLAN_LOAD')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_DAILY_PLAN_LOAD',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC DAILY PLAN LOAD JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                HeaderSoController::planLoadInsert();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC DAILY PLAN LOAD AT '.carbon::now());
            }else{
                $this->info('SYNC DAILY PLAN LOAD SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
