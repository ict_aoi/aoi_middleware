<?php

namespace App\Console\Commands;
use App\Http\Controllers\Integration\DataCuttingController;

use App\Models\Scheduler;
use Carbon\Carbon;

use Illuminate\Console\Command;

class DailySyncDataCutting extends Command
{
    protected $signature = 'dailySyncDataCutting:insert';
    protected $description = 'Insert Daily Data Cutting';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_DAILY_DATA_CUTTING')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC DAILY DATA CUTTING AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            DataCuttingController::dailySync();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY DATA CUTTING AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_DAILY_DATA_CUTTING')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_DAILY_DATA_CUTTING',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC DAILY DATA CUTTING JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                DataCuttingController::dailySync();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC DAILY DATA CUTTING AT '.carbon::now());
            }else{
                $this->info('SYNC DAILY DATA CUTTING SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
