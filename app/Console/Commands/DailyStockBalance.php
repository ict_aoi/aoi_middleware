<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Integration\FinishGoodsController;

use App\Models\Scheduler;
use Carbon\Carbon;


class DailyStockBalance extends Command
{
    protected $signature = 'dailyStockBalance:insert';
    protected $description = 'Insert Daily Stock Balance';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_DAILY_STOCK_BALANCE')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC DAILY STOCK BALANCE AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            FinishGoodsController::StockBalanceInsert();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY STOCK BALANCE AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_DAILY_STOCK_BALANCE')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_DAILY_STOCK_BALANCE',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC DAILY STOCK BALANCE JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                FinishGoodsController::StockBalanceInsert();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC DAILY STOCK BALANCE AT '.carbon::now());
            }else{
                $this->info('SYNC DAILY STOCK BALANCE SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
