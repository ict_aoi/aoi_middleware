<?php  namespace App\Console\Commands\WMS;

use DB;
use File;
use Config;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\Wms\Mrp;
use App\Models\Wms\PoBuyer;
use App\Models\Wms\Temporary;


class InsertMrp extends Command
{
    protected $signature    = 'wmsInsertMrp:daily';
    protected $description  = 'Daily Insert Wms MRP';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $location = Config::get('storage.wms_mrp');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);
        
        $is_schedule_on_going = Scheduler::where('job','SYNC_INSERT_WMS_MRP')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_INSERT_WMS_MRP',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC INSERT WMS MRP JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->insertJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE SYNC INSERT WMS MRP AT '.carbon::now());
        }else
        {
            $this->info('SYNC INSERT WMS MRP IS RUNNING');
        }
    }

    private function insertJob()
    {
        $data = Temporary::select('barcode as po_buyer')
        ->where('status','mrp')
        ->groupby('barcode')
        // ->orderBy('updated_at', 'asd')
        // ->take('300')
        ->get();

        $inserted_at = carbon::now();
        foreach ($data as $key => $value) 
        {
            try
            {
                db::beginTransaction();
                
                $po_buyer = PoBuyer::where('po_buyer',$value->po_buyer)->first();
                Mrp::where('po_buyer',$value->po_buyer)->delete();

                $mrp = DB::connection('wms_live')
                ->select(db::raw("SELECT * FROM get_mrp_cte(
                    '".$value->po_buyer."'
                    );"
                ));

                foreach ($mrp as $kmr => $mrp_val) 
                {
                    if($mrp_val->last_status_movement == 'receiving') $last_status_movement = 'MATERIAL DITERIMA DIGUDANG';
                    else if($mrp_val->last_status_movement == 'not receive') $last_status_movement = 'MATERIAL BELUM DITERIMA DIGUDANG';
                    else if($mrp_val->last_status_movement == 'in') $last_status_movement = 'MATERIAL SIAP SUPLAI';
                    else if($mrp_val->last_status_movement == 'expanse') $last_status_movement = 'EXPENSE';
                    else if($mrp_val->last_status_movement == 'out') $last_status_movement = 'MATERIAL SUDAH DISUPLAI';
                    else if($mrp_val->last_status_movement == 'out-handover') $last_status_movement = 'MATERIAL PINDAH TANGAN';
                    else if($mrp_val->last_status_movement == 'out-subcont') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE SUBCONT';
                    else if($mrp_val->last_status_movement == 'reject') $last_status_movement = 'REJECT';
                    else if($mrp_val->last_status_movement == 'qc-release') $last_status_movement = 'MATERIAL SUDAH RELEASE QC';
                    else if($mrp_val->last_status_movement == 'change') $last_status_movement = 'MATERIAL PINDAH LOKASI RAK';
                    else if($mrp_val->last_status_movement == 'hold') $last_status_movement = 'MATERIAL DITAHAN QC UNTUK PENGECEKAN';
                    else if($mrp_val->last_status_movement == 'adjustment' || $mrp_val->last_status_movement == 'adjusment') $last_status_movement = 'PENYESUAIAN QTY';
                    else if($mrp_val->last_status_movement == 'print') $last_status_movement = 'CETAK BARCODE';
                    else if($mrp_val->last_status_movement == 'reroute') $last_status_movement = 'REROUTE';
                    else if($mrp_val->last_status_movement == 'cancel item') $last_status_movement = 'CANCEL ITEM';
                    else if($mrp_val->last_status_movement == 'cancel order') $last_status_movement = 'CANCEL ORDER';
                    else if($mrp_val->last_status_movement == 'check') $last_status_movement = 'MATERIAL DALAM PENGECEKAN QC';
                    else if($mrp_val->last_status_movement == 'out-cutting') $last_status_movement = 'MATERIAL SUDAH DISUPLAI KE CUTTING';
                    else if($mrp_val->last_status_movement == 'relax') $last_status_movement = 'RELAX FABRIC';
                    else $last_status_movement = $mrp_val->last_status_movement;

                    if($po_buyer)
                    {
                        if($po_buyer->statistical_date) $statistical_date = $po_buyer->statistical_date;
                        else $statistical_date = $po_buyer->promise_date;
                    }else
                    {
                        $statistical_date   = null;
                    }

                    Mrp::Create([
                        'lc_date'                       => ($po_buyer? $po_buyer->lc_date : null),
                        'backlog_status'                => $mrp_val->backlog_status,
                        'statistical_date'              => $statistical_date,
                        'style'                         => $mrp_val->style,
                        'season'                        => $mrp_val->season,
                        'article_no'                    => $mrp_val->article_no,
                        'po_buyer'                      => $mrp_val->po_buyer,
                        'category'                      => $mrp_val->category,
                        'item_code'                     => $mrp_val->item_code,
                        'item_desc'                     => $mrp_val->item_desc,
                        'qty_need'                      => $mrp_val->qty_need,
                        'warehouse'                     => $mrp_val->warehouse,
                        'uom'                           => $mrp_val->uom,
                        'status'                        => $last_status_movement,
                        'location'                      => $mrp_val->last_locator_code,
                        'movement_date'                 => $mrp_val->last_movement_date,
                        'user_name'                     => $mrp_val->last_user_movement_name,
                        'qty_in'                        => $mrp_val->qty_available,
                        'qty_supply'                    => $mrp_val->qty_supply,
                        'qty_handover_not_received_yet' => $mrp_val->qty_handover_no_received_yet,
                        'supplier_name'                 => $mrp_val->supplier_name,
                        'document_no'                   => $mrp_val->document_no,
                        'remark'                        => $mrp_val->remark,
                        'system_log'                    => $mrp_val->system_log
                    ]);
                }

                Temporary::where([
                    ['barcode',$value->po_buyer],
                    ['status','mrp'],
                ])
                ->delete();

                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
            }
            
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
