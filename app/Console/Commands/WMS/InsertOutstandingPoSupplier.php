<?php namespace App\Console\Commands\WMS;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\Wms\Supplier;
use App\Models\Wms\PoSupplier;
use App\Models\Wms\MappingStock;

class InsertOutstandingPoSupplier extends Command
{
    protected $signature    = 'wmsOutstandingPoSupplier:insert';
    protected $description  = 'Daily Insert Outstanding Po Supplier Wms';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_INSERT_WMS_PO_SUPPLIER')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_INSERT_WMS_PO_SUPPLIER',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC INSERT WMS PO SUPPLIER JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->insertJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE SYNC INSERT WMS PO SUPPLIER AT '.carbon::now());
        }else
        {
            $this->info('SYNC INSERT WMS PO SUPPLIER IS RUNNING');
        }
    }

    private function insertJob()
    {
        $data = DB::connection('wms_live')
        ->select(db::raw("
            select * from master_po_supplier_erp_v 
            where period_po >= '1908'
            and c_order_id not in (
                    select c_order_id
                    from po_suppliers
                    where c_order_id is not null
                    GROUP BY c_order_id
            )
        "));


        $inserted_at = carbon::now();
        foreach ($data as $key => $value) 
        {
            try
            {
                db::beginTransaction();

                $c_order_id     = $value->c_order_id;
                $c_bpartner_id  = $value->c_bpartner_id;
                $document_no    = trim(strtoupper($value->document_no));
                $period_po      = trim(strtoupper($value->period_po));

                if($document_no == 'FREE STOCK')
                {
                    $c_bpartner_id          = 'FREE STOCK';
                    $supplier_code          = 'FREE STOCK';
                    $_mapping_stock         = MappingStock::where('document_number',$document_no)->first();
                    $type_stock_erp_code    = $_mapping_stock->type_stock_erp_code;
                    $type_stock             = $_mapping_stock->type_stock;
                    
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStock::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStock::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                } 

                $is_exists  = PoSupplier::where('document_no',$document_no)->first();
                $supplier   = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();

                if(!$is_exists)
                {
                    $po_supplier = PoSupplier::create([
                        'c_order_id'            => $c_order_id,
                        'c_bpartner_id'         => $c_bpartner_id,
                        'supplier_id'           => ($supplier ? $supplier->id : null),
                        'integration_date'      => $inserted_at,
                        'type_stock_erp_code'   => $type_stock_erp_code,
                        'type_stock'            => $type_stock,
                        'document_no'           => $document_no,
                        'periode_po'            => $period_po
                    ]);
                    $po_supplier_id = $po_supplier->id;
                }else
                {
                    $is_exists->c_bpartner_id   = trim(strtoupper($value->c_bpartner_id));
                    $is_exists->c_order_id      = $c_order_id;
                    $is_exists->supplier_id     = ($supplier ? $supplier->id : null);
                    $is_exists->save();
                    $po_supplier_id             = $is_exists->id;

                    //update auto alokasi 
                    $auto_allocations = $data->table('auto_allocations')
                                       ->where('document_no', $document_no)
                                       ->update(['c_order_id' => $c_order_id]);
                    //update allocation item 
                    $allocation_items = $data->table('allocation_items')
                                       ->where('document_no', $document_no)
                                       ->update(['c_order_id' => $c_order_id]);
                    //update detail material planning
                    $detail_material_planning_fabrics = $data->table('detail_material_planning_fabrics')
                                       ->where('document_no', $document_no)
                                       ->update(['c_order_id' => $c_order_id]);
                    //update material preparation fabric
                    $material_preparation_fabrics = $data->table('material_preparation_fabrics')
                                       ->where('document_no', $document_no)
                                       ->update(['c_order_id' => $c_order_id]);
                    //update stock
                    $material_stocks = $data->table('material_stocks')
                                       ->where('document_no', $document_no)
                                       ->update(['c_order_id' => $c_order_id]);
                    //update kedatangan 
                    $material_arrivals = $data->table('material_arrivals')
                                       ->where('document_no', $document_no)
                                       ->update(['c_order_id' => $c_order_id]);
                                       
                }

                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
