<?php

namespace App\Console\Commands\WMS;


use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Scheduler;
use App\Http\Controllers\WMS\AccessoriesReportMaterialMonitoringController;

class DailyUpdateDashboardMonitoringMaterialACC extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyDashboardMonitoringMaterialACC:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update dashboard monitoring material ACC';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scheduler_planning = Scheduler::where('job','DAILY_MONITORING_MATERIAL_ACC')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_planning)){
            $this->setStatus($scheduler_planning,'ongoing');
            $this->setStartJob($scheduler_planning);

            $this->info('MOVE DATA TO MDWARE JOB AT '.carbon::now());
            AccessoriesReportMaterialMonitoringController::insertAllocationACC();
            $this->info('DONE MOVE DATA TO MDWARE JOB AT '.carbon::now());
            $this->info('CALCULATION DATA JOB AT '.carbon::now());
            AccessoriesReportMaterialMonitoringController::updateMonitoringMaterialACC();
            $this->info('DONE CALCULATION DATA JOB AT '.carbon::now());
            $this->info('DONE DAILY MONITORING MATERIAL JOB JOB AT '.carbon::now());

            // $this->info('DAILY INSERT DASHBOARD ERP JOB AT '.carbon::now());
            // AccessoriesReportMaterialMonitoringController::insertToDashboardErp();
            // $this->info('DONE DAILY INSERT DASHBOARD ERP JOB JOB AT '.carbon::now());

            $this->setStatus($scheduler_planning,'done');
            $this->setEndJob($scheduler_planning);
            
        }else{
            $is_schedule_on_going = Scheduler::where('job','DAILY_MONITORING_MATERIAL_ACC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'DAILY_MONITORING_MATERIAL_ACC',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler);
                
                $this->info('MOVE DATA TO MDWARE JOB AT '.carbon::now());
                AccessoriesReportMaterialMonitoringController::insertAllocationACC();
                $this->info('DONE MOVE DATA TO MDWARE JOB AT '.carbon::now());

                $this->info('CALCULATION DATA JOB AT '.carbon::now());
                AccessoriesReportMaterialMonitoringController::updateMonitoringMaterialACC();
                $this->info('DONE CALCULATION DATA JOB AT '.carbon::now());

                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                
            }else{
                $this->info('DAILY MONITORING MATERIAL JOB IS RUNNING');
            }
        }

        
        $this->info('Daily update has been successfully');
    }


    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
