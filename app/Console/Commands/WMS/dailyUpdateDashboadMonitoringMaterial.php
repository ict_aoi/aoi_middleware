<?php

namespace App\Console\Commands\WMS;

use DB;
use Carbon\Carbon;
use App\Models\Scheduler;
use App\Http\Controllers\WMS\FabricMaterialMonitoringController;
use Illuminate\Console\Command;

class dailyUpdateDashboadMonitoringMaterial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyDashboardMonitoringMaterialFab:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update dashboard monitoring material ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

         $is_schedule_on_going = Scheduler::where('job','SYNC_DASHBOARD_MONITORING_MATERIAL')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_DASHBOARD_MONITORING_MATERIAL',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            $this->doJob();
            $this->info('DAILY MONITORING MATERIAL FABRIC JOB AT '.carbon::now());
            $this->info('MOVE DATA TO MDWARE JOB AT '.carbon::now());
            FabricMaterialMonitoringController::insertAllocation();
            $this->info('DONE MOVE DATA TO MDWARE JOB AT '.carbon::now());
            $this->info('CALCULATION DATA JOB AT '.carbon::now());
            FabricMaterialMonitoringController::updateMonitoringMaterial();
            $this->info('DONE CALCULATION DATA JOB AT '.carbon::now());
            $this->info('SYNC INSERT TO ERP JOB AT '.carbon::now());
            FabricMaterialMonitoringController::insertToDashboardErp();
            $this->info('DONE SYNC INSERT TO ERP JOB AT '.carbon::now());
            $this->info('DONE DAILY MONITORING MATERIAL FABRIC JOB AT '.carbon::now());
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC DASHBOARD MONITORING FABRIC MATERIAL SEDANG BERJALAN');
        }

        
    }

    static function doJob()
    {
        
    } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
