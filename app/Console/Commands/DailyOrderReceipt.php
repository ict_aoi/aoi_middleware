<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Integration\IntegrationController;

use App\Models\Scheduler;
use Carbon\Carbon;

class DailyOrderReceipt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyOrderReceipt:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Order Receipt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $schedular = Scheduler::where('job', 'SYNC_DAILY_ORDER_RECEIPT')
                            ->where('status', 'queue')
                            ->first();

        if (!empty($schedular)) {
            $this->info('SYNC DAILY ORDER RECEIPT AT '.carbon::now());
            $this->setStatus($schedular, 'ongoing');
            $this->setStartJob($schedular);
            IntegrationController::OrderReceipt();
            $this->setStatus($schedular, 'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY ORDER RECEIPT AT '.carbon::now());
        }else {
            $is_schedule_on_going = Scheduler::where('job', 'SYNC_DAILY_ORDER_RECEIPT')
                                            ->where('status', 'ongoing')
                                            ->exists();

            $data_schedule_on_going = Scheduler::where('job', 'SYNC_DAILY_ORDER_RECEIPT')
                                            ->where('status', 'ongoing')
                                            ->first();

            if (!$is_schedule_on_going) {
                $this->syncJob();
            } else {
                $date_on_going = $data_schedule_on_going->created_at;
                $now = Carbon::now();

                $length = $now->diffInHours($date_on_going);
                if ($length>1) {
                    Scheduler::where('job', 'SYNC_DAILY_ORDER_RECEIPT')
                                            ->where('status', 'ongoing')
                                            ->update([
                                                'status' => 'onrestart'
                                            ]);
                    $this->handle();
                }else {
                    $this->info('SYNC DAILY ORDER RECEIPT SEDANG BERJALAN');
                }

            }
            
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }

    private function syncJob()
    {
        $new_scheduler = Scheduler::create([
            'job' => 'SYNC_DAILY_ORDER_RECEIPT',
            'status' => 'ongoing'
        ]);
        $this->info('SYNC DAILY ORDER RECEIPT JOB AT'.Carbon::now());
        $this->setStartJob($new_scheduler);
        IntegrationController::OrderReceipt();
        $this->setStatus($new_scheduler, 'done');
        $this->setEndJob($new_scheduler);
        $this->info('DONE SYNC DAILY ORDER RECEIPT AT '.carbon::now());
    }
}
