<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\Integration\AttendanceController;

use App\Models\Scheduler;

class AttendanceScheduleAoi2 extends Command
{
    
    protected $signature = 'dailyAttendanceAoi2:insert';
    protected $description = 'Insert Attendance Daily AOI 2';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_ATTENDANCE_AOI_2')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC ATTENDANCE JOB AOI 2 AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            AttendanceController::daily_sync_aoi_2();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC ATTENDANCE JOB AOI 2 AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_ATTENDANCE_AOI_2')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_ATTENDANCE_AOI_2',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC ATTENDANCE AOI 2 JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                AttendanceController::daily_sync_aoi_2();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC ATTENDANCE AOI 2 JOB AT '.carbon::now());
            }else{
                $this->info('SYNC ATTENDANCE SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
