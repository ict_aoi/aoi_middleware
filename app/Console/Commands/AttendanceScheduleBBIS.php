<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\Integration\AttendanceController;

use App\Models\Scheduler;

class AttendanceScheduleBBIS extends Command
{
    
    protected $signature = 'dailyAttendanceBBIS:insert';
    protected $description = 'Insert Attendance Daily BBIS';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_ATTENDANCE_BBIS')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC ATTENDANCE JOB BBIS AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            AttendanceController::daily_sync_bbis();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC ATTENDANCE JOB BBIS AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_ATTENDANCE_BBIS')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_ATTENDANCE_BBIS',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC ATTENDANCE BBIS JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                AttendanceController::daily_sync_bbis();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC ATTENDANCE BBIS JOB AT '.carbon::now());
            }else{
                $this->info('SYNC ATTENDANCE SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
