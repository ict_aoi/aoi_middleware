<?php

namespace App\Console\Commands;
use App\Http\Controllers\Integration\DataCuttingLiveController;

use App\Models\Scheduler;
use Carbon\Carbon;

use Illuminate\Console\Command;

class DailySyncDataCuttingLive extends Command
{
    protected $signature = 'dailySyncDataCuttingLive:insert';
    protected $description = 'Insert Daily Data Cutting Live';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_DAILY_DATA_CUTTING_LIVE')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC DAILY DATA CUTTING LIVE AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            DataCuttingLiveController::dailySync();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC DAILY DATA CUTTING LIVE AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_DAILY_DATA_CUTTING_LIVE')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_DAILY_DATA_CUTTING_LIVE',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC DAILY DATA CUTTING LIVE JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                DataCuttingLiveController::dailySync();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC DAILY DATA CUTTING LIVE AT '.carbon::now());
            }else{
                $this->info('SYNC DAILY DATA CUTTING LIVE SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
