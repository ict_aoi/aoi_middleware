<?php

namespace App\Console\Commands;
use App\Http\Controllers\Integration\OutCuttingController;

use App\Models\Scheduler;
use Carbon\Carbon;

use Illuminate\Console\Command;

class OutputSyncDistribusi extends Command
{
    protected $signature = 'outputSyncDistribusi:insert';
    protected $description = 'Insert Output Cutting';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_OUTPUT_CUTTING')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC OUTPUT CUTTING AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            OutCuttingController::outputSync();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC OUTPUT CUTTING AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_OUTPUT_CUTTING')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_OUTPUT_CUTTING',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC OUTPUT CUTTING JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                OutCuttingController::outputSync();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC OUTPUT CUTTING AT '.carbon::now());
            }else{
                $this->info('SYNC OUTPUT CUTTING SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
