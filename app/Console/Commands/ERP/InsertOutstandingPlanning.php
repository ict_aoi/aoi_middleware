<?php namespace App\Console\Commands\ERP;

use Carbon\Carbon;
use Illuminate\Console\Command;

use DB;

use App\Models\Scheduler;
use App\Models\Erp\PlanningAllocation;

class InsertOutstandingPlanning extends Command
{
    protected $signature = 'OutstandingPlanningErp:insert';
    protected $description = 'Insert Outstanding Planning Erp';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_INSERT_OUTSTANDING_PLANNING')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going){
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_INSERT_OUTSTANDING_PLANNING',
                'status' => 'ongoing'
            ]);
            $this->info('SYNC INSERT OUTSTANDING PLANNING JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->insertJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE SYNC INSERT OUTSTANDING PLANNING AT '.carbon::now());
        }else{
            $this->info('SYNC INSERT OUTSTANDING PLANNING SEDANG BERJALAN');
        }
    }

    private function insertJob()
    {
        $data       = DB::connection('erp_live') //dev_erp //erp
        ->table('rma_wms_planning_alokasi_gab')
        ->where('status_lc','PR')
        /*->WHERE([
            ['c_order_id','1493981'],
            ['item_id','1420697'],
            ['m_warehouse_id','1000001'],
            ['qty','493.882200'],
        ])*/
        ->get();

        
        try
        {
            db::beginTransaction();

            $inserted_at = carbon::now();
            foreach ($data as $key => $value) 
            {
                $is_exists = PlanningAllocation::where([
                    ['c_order_id',$value->c_order_id],
                    ['item_id',$value->item_id],
                    ['m_warehouse_id',$value->m_warehouse_id],
                    ['po_buyer',$value->po_buyer],
                ])
                ->first();

                if(!$is_exists)
                {
                    PlanningAllocation::FirstOrCreate([
                        'c_order_id'        => $value->c_order_id,
                        'item_id'           => $value->item_id,
                        'c_bpartner_id'     => $value->c_bpartner_id,
                        'supplier_name'     => $value->supplier_name,
                        'item_code'         => $value->item_code,
                        'document_no'       => $value->document_no,
                        'po_buyer'          => $value->po_buyer,
                        'm_warehouse_id'    => $value->m_warehouse_id,
                        'warehouse'         => $value->warehouse,
                        'uom'               => $value->uom,
                        'category'          => $value->category,
                        'qty'               => $value->qty,
                        'isfabric'          => $value->isfabric,
                        'status_lc'         => $value->status_lc,
                        'created'           => $inserted_at,
                        'updated'           => $inserted_at,
                        'isintegrate'       => false,
                        'source'            => 'MIDDLEWARE AOI',
                    ]);
                }else
                {
                    $is_exists->c_bpartner_id       = $value->c_bpartner_id;
                    $is_exists->supplier_name       = $value->supplier_name;
                    $is_exists->qty                 = $value->qty;
                    $is_exists->source              = 'UPDATED BY MIDDLEWARE AOI';
                    $is_exists->save();
               
                }
            }
            
            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
