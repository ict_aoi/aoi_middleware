<?php

namespace App\Console\Commands;
use App\Http\Controllers\Integration\OutCuttingV2Controller;

use App\Models\Scheduler;
use Carbon\Carbon;

use Illuminate\Console\Command;

class OutputSyncDistribusiV2 extends Command
{
    protected $signature = 'outputSyncDistribusiV2:insert';
    protected $description = 'Insert Output Cutting V2';

    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_OUTPUT_CUTTING_V2')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC OUTPUT CUTTING V2 AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            OutCuttingV2Controller::outputSync();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC OUTPUT CUTTING V2 AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_OUTPUT_CUTTING_V2')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_OUTPUT_CUTTING_V2',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC OUTPUT CUTTING V2 JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                OutCuttingV2Controller::outputSync();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC OUTPUT CUTTING V2 AT '.carbon::now());
            }else{
                $this->info('SYNC OUTPUT CUTTING V2 SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
