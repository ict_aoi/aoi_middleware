<?php

namespace App\Models\Cutting;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DataCuttingLive extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $connection   = 'cdms_live';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $table        = 'data_cuttings';
    protected $fillable     = ['documentno', 'style', 'job_no', 'po_buyer', 'customer', 'destination', 'product', 'ordered_qty', 'part_no', 'material', 'color_name', 'product_category', 'cons', 'fbc', 'cutting_date', 'uom', 'is_piping', 'custno', 'statistical_date', 'lc_date', 'upc', 'color_code_raw_material', 'width_size', 'code_category_raw_material', 'desc_category_raw_material', 'desc_produksi', 'mo_updated', 'ts_lc_date', 'articleno', 'size_finish_good', 'size_category', 'color_finish_good', 'warehouse', 'desc_mo', 'status_ori', 'mo_created', 'promised_date', 'season', 'article_name', 'garment_type', 'queu', 'deleted_at', 'item_id','is_recycle'];
}
