<?php

namespace App\Models\Cutting;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $connection   = 'cdms_dev';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $table        = 'infos';
    protected $fillable     = ['type', 'cutting_date', 'style', 'articleno', 'po_buyer', 'documentno', 'desc', 'deleted_at', 'factory_id'];
}
