<?php namespace App\Models;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['nik','finger_id','status','attendance_date','_attendance_date'];
}
