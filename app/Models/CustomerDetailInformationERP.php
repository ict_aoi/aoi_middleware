<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDetailInformationERP extends Model
{
    protected $connection = 'erp_live';
    protected $guarded = ['id'];
    protected $table = 'bw_so_customers_detail';
}
