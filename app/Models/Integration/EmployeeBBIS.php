<?php namespace App\Models\Integration;

use Illuminate\Database\Eloquent\Model;

class EmployeeBBIS extends Model
{
    protected $connection = 'absensi_bbis';
    protected $primaryKey = 'nik';
    protected $guarded = ['nik'];
    protected $table = 'm_employee';
    public $incrementing = true;

    protected $fillable = ['nik','name','department','position','factory','status','created_at','nonactive_date','reactive_date','updated_at','is_updated'];
}
