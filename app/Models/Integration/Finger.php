<?php namespace App\Models\Integration;

use Illuminate\Database\Eloquent\Model;

class Finger extends Model
{
    protected $connection = 'finger_aoi_1';
    protected $guarded = ['finger_id'];
    protected $table = 'profinger_log';
}
