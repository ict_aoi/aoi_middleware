<?php namespace App\Models\Integration;

use Illuminate\Database\Eloquent\Model;

class FingerAOI2 extends Model
{
    protected $connection = 'finger_aoi_2';
    protected $guarded = ['finger_id'];
    protected $table = 'profinger_log';
}
