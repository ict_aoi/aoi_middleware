<?php namespace App\Models\Integration;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $connection = 'absensi';
    protected $guarded = ['id'];
    protected $table = 'm_absensi';
    public $incrementing = true;

    protected $fillable = ['nik','created_time','action','date','status','created_by','finger_id'];
    
}
