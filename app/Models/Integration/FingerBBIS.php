<?php namespace App\Models\Integration;

use Illuminate\Database\Eloquent\Model;

class FingerBBIS extends Model
{
    protected $connection = 'finger_bbis';
    protected $guarded = ['finger_id'];
    protected $table = 'profinger_log';
}
