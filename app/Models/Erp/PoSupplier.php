<?php namespace App\Models\Erp;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class PoSupplier extends Model
{
    use Uuids;
    public $incrementing        = false;
    protected $guarded          = ['c_order_id'];
    protected $connection       = 'erp_live';
    protected $table            = 'wms_po_supplier';
    protected $fillable         = ['documentno','c_bpartner_id','c_order_id','supplier_code','supplier_name','created_at','type_po','m_warehouse_id','period_po'];
}
