<?php namespace App\Models\Erp;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class PlanningAllocation extends Model
{
    use Uuids;
    public $timestamps          = false;
    public $incrementing        = false;
    protected $primaryKey       = 'uuid';
    protected $guarded          = ['uuid'];
    protected $connection       = 'erp_live';
    protected $table            = 'rma_wms_planning_alokasi';
    protected $fillable         = ['c_order_id'
        ,'uuid'
        ,'item_id'
        ,'c_bpartner_id'
        ,'supplier_name'
        ,'item_code'
        ,'document_no'
        ,'po_buyer'
        ,'m_warehouse_id'
        ,'warehouse'
        ,'uom'
        ,'category'
        ,'qty'
        ,'isfabric'
        ,'status_lc'
        ,'created'
        ,'updated'
        ,'source'
        ,'isintegrate'
    ];
}
