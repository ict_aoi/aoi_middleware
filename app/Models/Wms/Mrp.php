<?php namespace App\Models\Wms;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Mrp extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $connection   = 'wms_live';
    protected $table        = 'mrps';
    protected $dates        = ['created_at','updated_at','deleted_at','lc_date'];
    protected $guarded      = ['id'];
    protected $fillable     = ['item_code', 
        'item_desc',
        'po_buyer',
        'category',
        'backlog_status',
        'article_no',
        'uom',
        'style',
        'job_order',
        'qty_need',
        'status',
        'location',
        'statistical_date',
        'movement_date',
        'user_name',
        'warehouse',
        'qty_in',
        'qty_supply',
        'qty_handover_not_received_yet',
        'remark',
        'note',
        'document_no',
        'system_log',
        'supplier_name',
        'season',
        'lc_date'
    ];
}
