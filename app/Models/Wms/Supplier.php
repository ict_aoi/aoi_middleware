<?php namespace App\Models\Wms;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $connection   = 'wms_live';
    protected $table        = 'suppliers';
    protected $dates        = ['created_date','requirement_date','lc_date','statistical_date','promise_date'];
    protected $fillable     = ['supplier_code'
        ,'supplier_name'
        ,'c_bpartner_id'
        ,'type_supplier'
    ];
}
