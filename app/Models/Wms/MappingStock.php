<?php namespace App\Models\Wms;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MappingStock extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $connection   = 'wms_live';
    protected $table        = 'mapping_stocks';
    protected $dates        = ['created_date','requirement_date','lc_date','statistical_date','promise_date'];
    protected $fillable     = ['type_stock','type_stock_erp_code','document_number','deleted_at'];
}
