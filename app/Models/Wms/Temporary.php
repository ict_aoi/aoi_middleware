<?php namespace App\Models\Wms;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Temporary extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $connection   = 'wms_live';
    protected $table        = 'temporaries';
    protected $dates        = ['created_at'];
    protected $guarded      = ['id'];
    protected $fillable     = ['barcode',
        'status',
        'user_id',
        'warehouse',
        'string_1',
        'string_2',
        'string_3',
        'string_4',
        'string_5',
        'double_1',
        'double_2',
        'double_3',
        'double_4',
        'double_5',
        'integer_1',
        'integer_2',
        'integer_3',
        'integer_4',
        'integer_5',
        'text_1',
        'text_2',
        'text_3',
        'text_4',
        'text_5',
        'date_1',
        'date_2',
        'date_3',
        'date_4',
        'date_5',
        'datetime_1',
        'datetime_2',
        'datetime_3',
        'datetime_4',
        'datetime_5',
        'time_1',
        'time_2',
        'time_3',
        'time_4',
        'time_5',
        'boolean_1',
        'boolean_2',
        'boolean_3',
        'boolean_4',
        'boolean_5',
        'created_at',
        'updated_at',
    ];
}
