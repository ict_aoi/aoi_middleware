<?php namespace App\Models\Wms;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class PoSupplier extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $connection   = 'wms_live';
    protected $table        = 'po_suppliers';
    protected $dates        = ['created_date','requirement_date','lc_date','statistical_date','promise_date'];
    protected $fillable     = ['c_order_id'
        ,'c_bpartner_id'
        ,'type_stock'
        ,'type_stock_erp_code'
        ,'supplier_id'
        ,'integration_date'
        ,'document_no'
        ,'periode_po'
    ];
}
