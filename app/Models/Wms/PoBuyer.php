<?php namespace App\Models\Wms;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class PoBuyer extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $connection   = 'wms_live';
    protected $table        = 'po_buyers';
    protected $dates        = ['created_date','requirement_date','lc_date','statistical_date','promise_date'];
    protected $fillable     = ['po_buyer',
        'type_po',
        'lc_date',
        'destination',
        'is_po_subcont',
        'is_ordering_for_japan_china',
        'requirement_date',
        'statistical_date',
        'promise_date',
        'is_changed',
        'season',
        'cancel_user_id',
        'cancel_date',
        'cancel_reason',
        'warehouse_production_id',
        'brand',
        'type_stock',
        'type_stock_erp_code'
    ];
}
