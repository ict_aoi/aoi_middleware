<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Packingin extends Model
{
    use Uuids;
    public $incrementing        = false;
    protected $guarded          = ['id'];
    protected $table            = 'packing_in';
    protected $fillable         = ['po_number','plan_ref_number','upc','buyer_item','manufacturing_size','qty','checkin','plan_stuffing','created_at','updated_at','deleted_at','factory_id'];
}
