<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class PpcBalc extends Model
{
    use Uuids;
    public $incrementing        = false;
    protected $guarded          = ['id'];
    protected $table            = 'ppc_balance_production';
    protected $fillable         = ['po_number','plan_ref_number','plan_stuffing','podd_check','qty_pcs','qty_ctn','cbm','sewing_line','fwd','material_status','cutting_output','cutting_bal','loading_output','loading_bal','sewing_out','sewing_bal','folding_out','folding_bal','set_output','set_bal','storage_out','storage_bal','shipment_status','date_show','created_at','updated_at','deleted_at','factory_id','new_qty','smv','ship_out','ship_bal','style','rasio'];
}
