<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Packingout extends Model
{
    use Uuids;
    public $incrementing        = false;
    protected $guarded          = ['id'];
    protected $table            = 'packing_out';
    protected $fillable         = ['po_number','plan_ref_number','upc','buyer_item','manufacturing_size','qty','checkout','plan_stuffing','created_at','updated_at','deleted_at','factory_id'];
}
