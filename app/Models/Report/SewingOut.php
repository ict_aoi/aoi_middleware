<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class SewingOut extends Model
{
    use Uuids;
    public $incrementing        = false;
    protected $guarded          = ['id'];
    protected $table            = 'sewing_out';
    protected $fillable         = ['po_number','style','article','size','qty','line_id','factory_id','checkout','created_at','updated_at','deleted_at','plan_stuffing','line_name'];
}
