<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class SewingIn extends Model
{
    use Uuids;
    public $incrementing        = false;
    protected $guarded          = ['id'];
    protected $table            = 'sewing_in';
    protected $fillable         = ['po_number','style','article','size','qty','line_id','factory_id','checkin','created_at','updated_at','deleted_at','plan_stuffing','line_name'];
}
