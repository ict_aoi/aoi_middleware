<?php

namespace App\Models\Distribusi;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class OutputCutting extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $connection   = 'sds_live';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $table        = 'out_cutting_pcs';
    protected $fillable     = ['style', 'set_type', 'poreference', 'article', 'cut_num', 'start_num', 'end_num', 'qty', 'deleted_at', 'processing_fact', 'season', 'size', 'last_scan_at', 'user_id', 'doc_type_id'];
}