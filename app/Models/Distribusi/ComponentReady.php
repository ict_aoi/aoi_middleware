<?php

namespace App\Models\Distribusi;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ComponentReady extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $connection   = 'sds_live';
    protected $guarded      = ['id'];
    protected $dates        = ['created_at', 'updated_at'];
    protected $table        = 'component_ready';
    protected $fillable     = ['style', 'set_type', 'poreference', 'article', 'cut_num', 'start_num', 'end_num', 'qty', 'deleted_at', 'processing_fact', 'season', 'size', 'last_scan_at', 'component_ready', 'component_complete', 'is_ready', 'location', 'gsd_smv', 'total_smv'];
}