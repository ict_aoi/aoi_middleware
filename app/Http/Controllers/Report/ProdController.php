<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

use App\Models\Report\PpcBalc;

class ProdController extends Controller
{
    public function __construct(){
        ini_set('max_execution_time', 1800);
    }

    public function cronData(){
        $start = Carbon::now()->format('Y-m-d');
        $finish = Carbon::now()->addDays(31)->format('Y-m-d');

        $postuf = DB::connection('fgms_aoi')->table('ns_ppc_balance')
                            ->whereBetween('plan_stuffing',[$start,$finish])
                            ->orderBy('plan_stuffing','asc')
                            ->get();
        try {
            DB::begintransaction();
                foreach ($postuf as $pos) {
                    $check = $this->checkPO($pos->po_number,$pos->plan_ref_number);
                    $fgmas = $this->poMaster($pos->po_number);
                    $lineout = $this->lineOut($pos->po_number);
                    $wms = $this->wmsMtr($pos->po_number);
                    $cdms = $this->cdmsOutput($pos->po_number);
                    
                    switch ($fgmas->uom) {
                        case 'SETS':
                            
                            $sew_load = floor($lineout['qty_load']/2);
                            $sew_out = floor($lineout['qty_sewing']/2);
                            $sew_fol = floor($lineout['qty_folding']/2);

                            $set_out = $lineout['qty_sets'];
                            $set_bal = $lineout['qty_sets']-$fgmas->new_qty;
                        break;

                        case 'PCS':
                            
                            $sew_load = $lineout['qty_load'];
                            $sew_out = $lineout['qty_sewing'];
                            $sew_fol = $lineout['qty_folding'];

                            $set_out = null;
                            $set_bal = null;
                        break;
                            
                    }


                    if ($pos->ship_qty==$pos->tot_qty) {
                        $status_ship = "CLOSED";
                    }else if ($pos->ship_qty<$pos->tot_qty){
                        $status_ship = "OPEN";
                    }else{
                        $status_ship = "CLOSED";
                    }

                    $dataIn = array(
                        'po_number'=>$pos->po_number,
                        'plan_ref_number'=>$pos->plan_ref_number,
                        'plan_stuffing'=>$pos->plan_stuffing,
                        'podd_check'=>$fgmas->podd_check,
                        'new_qty'=>$fgmas->new_qty,
                        'qty_pcs'=>$pos->tot_qty,
                        'qty_ctn'=>$pos->tot_ctn,
                        'cbm'=>$pos->tot_cbm,
                        'sewing_line'=>$lineout['line'],
                        'fwd'=>$pos->fwd,
                        'material_status'=>$wms,
                        'cutting_output'=>$cdms,
                        'cutting_bal'=>($cdms-$fgmas->new_qty),
                        'loading_output'=>$sew_load,
                        'loading_bal'=>($sew_load-$fgmas->new_qty),
                        'sewing_out'=>$sew_out,
                        'sewing_bal'=>($sew_out-$fgmas->new_qty),
                        'folding_out'=>$sew_fol,
                        'folding_bal'=>($sew_fol-$fgmas->new_qty),
                        'set_output'=>$set_out,
                        'set_bal'=>$set_bal,
                        'storage_out'=>$pos->inv_qty,
                        'storage_bal'=>($pos->inv_qty-$pos->tot_qty),
                        'ship_out'=>$pos->ship_qty,
                        'ship_bal'=>($pos->ship_qty-$pos->tot_qty),
                        'shipment_status'=>$status_ship,
                        'date_show'=>Carbon::now()->format('Y-m-d'),
                        'created_at'=>Carbon::now(),
                        'factory_id'=>$pos->factory_id,
                        'smv'=>round($lineout['smv_balc'],4),
                        'style'=>$fgmas->style,
                        'rasio'=>$pos->rasio
                    );
                    // dd($dataIn);
                    PpcBalc::firstorCreate($dataIn);
                }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        

        
    }

    private function checkPO($ponumber,$planref){
        $cek = PpcBalc::where('po_number',$ponumber)->where('plan_ref_number',$planref)->first();

        if ($cek!=null) {
            PpcBalc::where('id',$cek->id)->delete();
        }

        return true;
    }

    private function poMaster($ponumber){
        $po = DB::connection('fgms_aoi')->table('po_master_sync')
                                ->where('po_number',$ponumber)
                                ->whereNull('deleted_at')
                                ->select('po_number','podd_check','style','new_qty','uom')->first();

        return $po;
    }

    

    private function wmsMtr($ponumber){
        $pom = DB::connection('erp_live')->table('rma_mat_dashboard')->where('po_buyer',$ponumber)->first();

        return isset($pom->fabric_status) ? $pom->fabric_status : "OPEN"; 
    }

    private function cdmsOutput($ponumber){
         $poct = DB::connection('cdms_live')->table('dd_output_cutting_v2')->where('po_buyer',$ponumber)->first();

         return isset($poct->qty_output) ? $poct->qty_output : 0;
    }

    

    private function lineOut($ponumber){
        $polo = DB::connection('line_aoi')->table('ns_line_ppc')
                                ->where('poreference',$ponumber)->first();

        $sets = DB::connection('line_aoi')->table('ns_out_sets')
                                ->where('poreference',$ponumber)->first();

        $order = DB::connection('line_aoi')->table('ns_smv_order')
                                ->where('poreference',$ponumber)->first();
        $out = DB::connection('line_aoi')->table('ns_smv_output')
                                ->where('poreference',$ponumber)->first();

        return array(
                    'line'=>isset($polo->line) ? $polo->line : null,
                    'qty_load'=>isset($polo->qty_load) ? $polo->qty_load : 0,
                    'qty_sewing'=>isset($polo->qty_sewing) ? $polo->qty_sewing : 0,
                    'qty_folding'=>isset($polo->qty_folding) ? $polo->qty_folding : 0,
                    'qty_sets'=>isset($sets->qty) ? $sets->qty : null,
                    'smv_balc'=>(isset($out->smv_out) ? $out->smv_out : 0) - (isset($order->smv) ? $order->smv : 0)
                );
    }

    public function cekPPC(){
       

        $now = Carbon::now()->format('Y-m-d');

        

        $ppc = PpcBalc::where('date_show',$now)->get();
        try {
            foreach ($ppc as $ppc) {
                $postuf = DB::connection('fgms_aoi')->table('ns_ppc_balance')
                                ->where('po_number',$ppc->po_number)
                                ->where('plan_ref_number',$ppc->plan_ref_number)
                                ->orderBy('plan_stuffing','asc')
                                ->exists();
                if (!$postuf) {
                    PpcBalc::where('id',$ppc->id)->delete();
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function clean(){
        

        $now = Carbon::now()->subDays(1)->format('Y-m-d');

        

        $ppc = PpcBalc::where('date_show',$now)->delete();
    }
}
