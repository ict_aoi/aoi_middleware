<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

use App\Models\Report\Packingin;
use App\Models\Report\Packingout;
use App\Models\Report\SewingIn;
use App\Models\Report\SewingOut;

class ReportController extends Controller
{
    public function __construct(){
        ini_set('max_execution_time', 1800);
    }



    public function packingIn(){
   
    	
    
    	try {
            DB::begintransaction();
    	
	    		$datain = DB::connection('fgms_aoi')->table('ns_pack_in')->get();
          
	    		foreach ($datain as $dtin) {
	    			$in = array(
	    				'po_number'=>$dtin->po_number,
	    				'plan_ref_number'=>$dtin->plan_ref_number,
	    				'upc'=>$dtin->upc,
	    				'buyer_item'=>$dtin->buyer_item,
	    				'manufacturing_size'=>$dtin->manufacturing_size,
	    				'qty'=>$dtin->qty,
	    				'checkin'=>$dtin->created_at,
                        'plan_stuffing'=>$dtin->plan_stuffing,
                        'created_at'=>Carbon::now(),
                        'factory_id'=>$dtin->factory_id
	    			);
                    Packingin::firstOrCreate($in);
	    		}

                

            DB::commit();
    	} catch (Exception $ex) {
    		DB::rollBack();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
    	}
    	
    }

    public function packingout(){
   
        
    
        try {
            DB::begintransaction();
        
                

                $dataout = DB::connection('fgms_aoi')->table('ns_pack_out')->get();

                foreach ($dataout as $dtout) {
                    $out = array(
                        'po_number'=>$dtout->po_number,
                        'plan_ref_number'=>$dtout->plan_ref_number,
                        'upc'=>$dtout->upc,
                        'buyer_item'=>$dtout->buyer_item,
                        'manufacturing_size'=>$dtout->manufacturing_size,
                        'qty'=>$dtout->qty,
                        'checkout'=>$dtout->created_at,
                        'plan_stuffing'=>$dtout->plan_stuffing,
                        'created_at'=>Carbon::now(),
                        'factory_id'=>$dtout->factory_id
                    );
                    Packingout::firstOrCreate($out);
                }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    private function listPo($date){
       

        $array = [];
        $fgms = DB::connection('fgms_aoi')->table('po_stuffing')
                                                    ->whereNull('deleted_at')
                                                    ->where('plan_stuffing',$date)
                                                    ->groupBy('po_number')
                                                    ->select('po_number')
                                                    ->get();
        foreach ($fgms as $fg) {
           array_push($array,$fg->po_number);
        }

        return $array;
    }

    public function sewingin(){
        $date = Carbon::now()->subDays(1)->format('Y-m-d');
        $polist = $this->listPo($date);

        $sewIn = DB::connection('line_live')->table('ns_distribusi_in')
                                                ->whereIn('poreference',$polist)
                                                ->get();
        try {
            DB::begintransaction();
                foreach ($sewIn as $in) {
                   $dtin = array(
                        'po_number'=>$in->poreference,
                        'style'=>$in->style,
                        'article'=>$in->article,
                        'size'=>$in->size,
                        'qty'=>$in->qty,
                        'line_id'=>$in->master_line_id,
                        'factory_id'=>$in->factory_id,
                        'checkin'=>$in->checkin,
                        'created_at'=>Carbon::now(),
                        'plan_stuffing'=>$date,
                        'line_name'=>$in->line_name
                    );


                   SewingIn::firstOrCreate($dtin);
                }
                

            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function sewingout(){
        $date = Carbon::now()->subDays(1)->format('Y-m-d');
        $polist = $this->listPo($date);

        $sewOut = DB::connection('line_live')->table('ns_folding_out')
                                                ->whereIn('poreference',$polist)
                                                ->get();
        try {
            DB::begintransaction();
                foreach ($sewOut as $out) {
                   $dtout = array(
                        'po_number'=>$out->poreference,
                        'style'=>$out->style,
                        'article'=>$out->article,
                        'size'=>$out->size,
                        'qty'=>$out->qty,
                        'line_id'=>$out->master_line_id,
                        'factory_id'=>$out->factory_id,
                        'checkout'=>$out->date,
                        'created_at'=>Carbon::now(),
                        'plan_stuffing'=>$date,
                        'line_name'=>$out->line_name
                    );

                   SewingOut::firstOrCreate($dtout);
                }
                

            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }
    }

}
