<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

    public static $url = "http://bima1.bbi-apparel.com:8080/ADInterface/services";
    //
    public static function callAPI($method, $url, $data){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Authorization: Basic'));
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "$data");
        ///////////
        switch ($method){
            case "POST":
               curl_setopt($ch, CURLOPT_POST, 1);
               if ($data)
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
               break;
            case "PUT":
               curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
               if ($data)
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);			 					
               break;
            default:
               if ($data)
                  $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        //////////
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Begin SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //End SSL
        // EXECUTE:
        $result = curl_exec($ch);
        if(!$result){die("Connection Failure");}
        curl_close($ch);
        return $result;
    }
}
