<?php

namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\CustomerDetailInformationERP;
use App\Models\DetailInformationAgv;

class PackingController extends Controller
{

    public function customerDetailInformation(Request $request){

        if(isset($request->po_number)) {

            $validator = Validator::make($request->all(), [
                'po_number' => 'required|string'
            ]);

            $po_num = array('SO-', $request->po_number, '-0');
            $po_numb = implode($po_num);

            $data_customers = CustomerDetailInformationERP::where('poreference', 'like', '%'.$request->po_number.'%')->get();
        }elseif (isset($request->poreference)) {

            $validator = Validator::make($request->all(), [
                'poreference' => 'required|string'
            ]);

            $data_customers = CustomerDetailInformationERP::where('poreference', $request->poreference)->get();
        }
        else
        {

            $validator = Validator::make($request->all(), [
                'lc_date_from' => 'required|date',
                'lc_date_to' => 'required|date|after_or_equal:lc_date_from'
            ]);

            $data_customers = CustomerDetailInformationERP::whereBetween('kst_lcdate',[$request->lc_date_from, $request->lc_date_to])->get();
        }


        if($validator->passes()){
            return response()->json(['data' => $data_customers, 'success' => 200]);
        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }

    }


    public function agvDetailInformation(Request $request){

            $validator = Validator::make($request->all(), [
                'barcode_id' => 'required|string'
            ]);

            $data = DetailInformationAgv::where('barcode_id',$request->barcode_id)->get();

        if($validator->passes()){
            return response()->json(['data' => $data, 'success' => 200]);
        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }

    }

}