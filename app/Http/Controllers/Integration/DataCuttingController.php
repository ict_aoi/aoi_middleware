<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\Cutting\Info;
use App\Models\Cutting\DataCutting;

class DataCuttingController extends Controller
{
    static function dailySync()
    {
        // set date range
        $from = date(Carbon::now()->subDays(7)->format('Y-m-d'));
        $to = date(Carbon::now()->addDays(7)->format('Y-m-d'));

        // get data erp
        $erp_csi_datas =  DB::connection('erp_live')->table('jz_csi_details')
                                ->whereBetween('datestartschedule', [$from, $to])
                                ->get();
        
        // get data cutting
        $cdms_cutting_datas =  DataCutting::whereBetween('cutting_date', [$from, $to])
                                    ->get();


        $is_exists = '';

        // start datebase excecution
        try 
        {
            DB::beginTransaction();
            
            // foreach data erp to check data cutting
            foreach ($erp_csi_datas as $key => $erp_csi_data) 
            {

                // set size_category
                $size_category = substr($erp_csi_data->size_fg, 0, 1);
                if($size_category == 'A') {
                    $size_category = 'A';
                } elseif($size_category == 'J') {
                    $size_category = 'J';
                }  else {
                    $size_category = 'I';
                }

                // set factory
                if($erp_csi_data->m_warehouse_id == '1000012') {
                    $warehouse = 2;
                } elseif($erp_csi_data->m_warehouse_id == '1000005') {
                    $warehouse = 1;
                }  else {
                    $warehouse = 0;
                }

                // get data cutting
                $data_is_exists = DataCutting::where('cutting_date', Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'))
                                        ->where('documentno', $erp_csi_data->documentno)
                                        ->where('style', $erp_csi_data->style)
                                        ->where('po_buyer', $erp_csi_data->po_buyer)
                                        ->where('articleno', $erp_csi_data->kst_articleno)
                                        ->where('size_finish_good', $erp_csi_data->size_fg)
                                        ->where('size_category', $size_category)
                                        ->where('part_no', $erp_csi_data->part_no)
                                        ->where('warehouse', $warehouse);
                
                // create variable to check data
                $is_exists_check = $data_is_exists->get();
                $is_exists = $is_exists_check->first();

                // check data is not null
                if(count($is_exists_check) > 0)
                {

                    // check data if update data
                    if((int)$erp_csi_data->qtyordered != (int)$is_exists->ordered_qty) {
                        
                        // set data to update
                        $data_for_update['ordered_qty'] = (int)$erp_csi_data->qtyordered;
                        $data_for_update['is_recycle'] = $erp_csi_data->recycle;

                        // insert table info status update
                        Info::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'size: '.$is_exists->size_finish_good.' || size category: '.$size_category.' || quantity from '.$is_exists->ordered_qty.' to '.$erp_csi_data->qtyordered,
                            'factory_id' => $warehouse,
                        ]);

                        // update data cutting
                        $data_is_exists->update($data_for_update);
                    }
                    else{
                        // insert table info status update
                        Info::FirstOrCreate([
                            'type' => 'update',
                            'cutting_date' => Carbon::parse($is_exists->cutting_date)->format('Y-m-d'),
                            'style' => $is_exists->style,
                            'articleno' => $is_exists->articleno,
                            'po_buyer' => $is_exists->po_buyer,
                            'documentno' => $is_exists->documentno,
                            'desc' => 'update is_recycle',
                            'factory_id' => $warehouse,
                        ]);

                        $data_for_update['is_recycle'] = $erp_csi_data->recycle;
                        $data_is_exists->update($data_for_update);
                    }

                } else {

                    // if data is null, insert new data
                    DataCutting::FirstOrCreate([
                        'documentno' => $erp_csi_data->documentno,
                        'style' => $erp_csi_data->style,
                        'job_no' => $erp_csi_data->job_no,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'customer' => $erp_csi_data->customer,
                        'destination' => $erp_csi_data->dest,
                        'product' => $erp_csi_data->product,
                        'ordered_qty' => (int)$erp_csi_data->qtyordered,
                        'part_no' => $erp_csi_data->part_no,
                        'material' => $erp_csi_data->material,
                        'color_name' => $erp_csi_data->color,
                        'product_category' => $erp_csi_data->product_category,
                        'cons' => $erp_csi_data->cons,
                        'fbc' => $erp_csi_data->fbc,
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'uom' => $erp_csi_data->uom,
                        'is_piping' => $erp_csi_data->ispiping,
                        'custno' => $erp_csi_data->custno,
                        'statistical_date' => $erp_csi_data->kst_statisticaldate,
                        'lc_date' => $erp_csi_data->kst_lcdate,
                        'upc' => $erp_csi_data->upc,
                        'color_code_raw_material' => $erp_csi_data->color_code_raw_material,
                        'width_size' => $erp_csi_data->width_size,
                        'code_category_raw_material' => $erp_csi_data->code_raw_material,
                        'desc_category_raw_material' => $erp_csi_data->desc_raw_material,
                        'desc_produksi' => $erp_csi_data->desc_produksi,
                        'mo_updated' => $erp_csi_data->updated == null ? null : Carbon::parse($erp_csi_data->updated)->format('Y-m-d H:i:s'),
                        'ts_lc_date' => null,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'size_finish_good' => $erp_csi_data->size_fg,
                        'size_category' => $size_category,
                        'color_finish_good' => null,
                        'warehouse' => $warehouse,
                        'desc_mo' => $erp_csi_data->description_mo,
                        'status_ori' => $erp_csi_data->status_ori,
                        'mo_created' => $erp_csi_data->created == null ? null : Carbon::parse($erp_csi_data->created)->format('Y-m-d H:i:s'),
                        'promised_date' => $erp_csi_data->datepromised,
                        'season' => $erp_csi_data->season,
                        'article_name' => null,
                        'garment_type' => null,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'item_id' => $erp_csi_data->item_id,
                        'is_recycle' => $erp_csi_data->recycle,
                    ]);

                    // insert to table info status create
                    Info::FirstOrCreate([
                        'type' => 'create',
                        'cutting_date' => Carbon::parse($erp_csi_data->datestartschedule)->format('Y-m-d'),
                        'style' => $erp_csi_data->style,
                        'articleno' => $erp_csi_data->kst_articleno,
                        'po_buyer' => $erp_csi_data->po_buyer,
                        'documentno' => $erp_csi_data->documentno,
                        'desc' => 'size: '.$erp_csi_data->size_fg.' || size category: '.$size_category,
                        'factory_id' => $warehouse,
                    ]);
                }                
            }

            // foreach data data cutting to check erp
            foreach ($cdms_cutting_datas as $key => $cdms_cutting_data) 
            {
                $check_count_data = 0;

                // loop data from erp
                for($increment_index = 0; $increment_index < count($erp_csi_datas); $increment_index++){

                    $warehouse_erp = 0;

                    if($erp_csi_datas[$increment_index]->m_warehouse_id == '1000005') {
                        $warehouse_erp = 1;
                    } elseif($erp_csi_datas[$increment_index]->m_warehouse_id == '1000012') {
                        $warehouse_erp = 2;
                    } else {
                        $warehouse_erp = 0;
                    }

                    // check if data not set
                    if($erp_csi_datas[$increment_index]->documentno == $cdms_cutting_data->documentno && Carbon::parse($erp_csi_datas[$increment_index]->datestartschedule)->format('Y-m-d') == $cdms_cutting_data->cutting_date && $erp_csi_datas[$increment_index]->style == $cdms_cutting_data->style && $erp_csi_datas[$increment_index]->po_buyer == $cdms_cutting_data->po_buyer && $erp_csi_datas[$increment_index]->kst_articleno == $cdms_cutting_data->articleno && $erp_csi_datas[$increment_index]->size_fg == $cdms_cutting_data->size_finish_good && $erp_csi_datas[$increment_index]->part_no == $cdms_cutting_data->part_no && $warehouse_erp == $cdms_cutting_data->warehouse) {
                        // data ada
                        $check_count_data = $check_count_data;
                    } else {
                        // data tidak ada
                        $check_count_data++;
                    }
                }

                if($check_count_data == count($erp_csi_datas)){
                    // delete data
                    DataCutting::where('cutting_date', Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'))
                        ->where('documentno', $cdms_cutting_data->documentno)
                        ->where('style', $cdms_cutting_data->style)
                        ->where('po_buyer', $cdms_cutting_data->po_buyer)
                        ->where('articleno', $cdms_cutting_data->articleno)
                        ->where('size_finish_good', $cdms_cutting_data->size_finish_good)
                        ->where('part_no', $cdms_cutting_data->part_no)
                        ->where('warehouse', $cdms_cutting_data->warehouse)->delete();
                    
                    // insert table info
                    Info::FirstOrCreate([
                        'type' => 'delete',
                        'cutting_date' => Carbon::parse($cdms_cutting_data->cutting_date)->format('Y-m-d'),
                        'style' => $cdms_cutting_data->style,
                        'articleno' => $cdms_cutting_data->articleno,
                        'po_buyer' => $cdms_cutting_data->po_buyer,
                        'documentno' => $cdms_cutting_data->documentno,
                        'desc' => 'size: '.$cdms_cutting_data->size_finish_good.' || size category: '.$size_category,
                        'factory_id' => $cdms_cutting_data->warehouse,
                    ]);
                }
            }

            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
