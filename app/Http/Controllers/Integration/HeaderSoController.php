<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class HeaderSoController extends Controller
{
    //
    static function planLoadInsert()
    {
        $data_erp = DB::connection('erp_live')
                        ->table('jz_fgms_header_so')
                        ->where(DB::raw('extract(year FROM po_stat_date_adidas)'), '>=', '2018')
                        ->where('new_qty', '>', 0)
                        ->whereNotNull('orderno')
                        ->whereNotNull('po_stat_date_adidas')
                        ->get();
        
        $data_fg = DB::connection('fgms_aoi')
                        ->table('po_summary_sync')
                        ->where(DB::raw('extract(year FROM po_stat_date_adidas)'), '>=', '2018')
                        ->whereNotNull('po_number')
                        ->whereNotNull('po_stat_date_adidas')
                        ->get();
        try {
            DB::begintransaction();

                foreach ($data_erp as $key => $value) {
                    
                    $orderno = trim($value->orderno);

                    if ($value->aoi_iscancelorder == 'Y') {
                        $is_cancel_order = true;
                    }else{
                        $is_cancel_order = false;
                    }

                    $check_exist = DB::connection('fgms_aoi')
                                    ->table('po_summary_sync')
                                    ->where('po_number', $orderno)
                                    ->exists();
                    
                    if ($check_exist) {
                            DB::connection('fgms_aoi')
                                ->table('po_summary_sync')
                                ->where('po_number', $orderno)
                                ->update([
                                    'podd_check' => $value->podd_check,
                                    'remark' => $value->remark,
                                    'sewing_place_update' => $value->warehouse,
                                    'stuffing_place_update' => $value->warehouse,
                                    'is_cancel_order' => $is_cancel_order,
                                    'product_category' => $value->product_category,
                                    'product_category_detail' => $value->product_category_detail,
                                    'updated_at' => Carbon::now()
                                ]);
                    }else{
                        DB::connection('fgms_aoi')
                            ->table('po_summary_sync')
                            ->insert([
                                'po_number' => $value->orderno,
                                'job' => $value->kst_joborder,
                                'season' => $value->kst_season,
                                'color' => $value->color,
                                'new_qty' => (int)$value->new_qty,
                                'po_stat_date_adidas' => $value->po_stat_date_adidas,
                                'podd_check' => $value->podd_check,
                                'customer_order_number' => $value->custorderno,
                                'customer_number' => $value->custno,
                                'country_name' => $value->country_name,
                                'remark' => $value->remark,
                                'order_type' => $value->order_type,
                                'kst_lcdate' => $value->kst_lcdate,
                                'style' => $value->style,
                                'article' => $value->article,
                                'document_type' => $value->document_type,
                                'crd' => $value->crd,
                                'sewing_place_update' => $value->warehouse,
                                'stuffing_place_update' => $value->warehouse,
                                'is_cancel_order' => $is_cancel_order,
                                'product_category' => $value->product_category,
                                'product_category_detail' => $value->product_category_detail,
                                'created_at' => Carbon::now()
                            ]);

                    }
                }

                // cek data yg sudah ketarik di fgms apakah masih ada di erp?
                foreach ($data_fg as $key2 => $val2) {
                    
                    $check_exist_erp = DB::connection('erp_live')
                                        ->table('c_order')
                                        ->where(DB::raw('extract(year FROM kst_statisticaldate)'), '>=', '2018')
                                        ->whereNotNull('poreference')
                                        ->where('poreference', $val2->po_number)
                                        ->where('issotrx', 'Y')
                                        ->exists();
                    
                    if (!$check_exist_erp) {
                        DB::connection('fgms_aoi')
                                ->table('po_summary_sync')
                                ->where('po_number', $val2->po_number)
                                ->update([
                                    'is_cancel_order' => true,
                                    'is_re_route' => true,
                                    'updated_at' => Carbon::now()
                                ]);
                    }
                }
            
            DB::commit();

        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);

    }
}
