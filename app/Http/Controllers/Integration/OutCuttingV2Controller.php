<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\Distribusi\OutputCutting;

class OutCuttingV2Controller extends Controller
{
    static function outputSync()
    {
        // get time
        $from = Carbon::now()->subMinutes(20);
        $to = Carbon::now()->addMinutes(10);

        // start datebase excecution
        try 
        {
            DB::beginTransaction();

            $limit_date = Carbon::now();

            $get_datas = DB::connection('sds_live')
                                ->table('bundle_info_new')
                                ->join('art_desc_new', function ($join) {
                                    $join->on('art_desc_new.style', '=', 'bundle_info_new.style')
                                            ->on('art_desc_new.season', '=', 'bundle_info_new.season')
                                            ->on('art_desc_new.part_num', '=', 'bundle_info_new.part_num')
                                            ->on('art_desc_new.part_name', '=', 'bundle_info_new.part_name');
                                })
                                ->select(DB::raw("bundle_info_new.style, art_desc_new.set_type, bundle_info_new.poreference, bundle_info_new.article, bundle_info_new.cut_num, bundle_info_new.start_num, bundle_info_new.end_num, bundle_info_new.qty, bundle_info_new.processing_fact, bundle_info_new.season, bundle_info_new.size, max(bundle_info_new.distrib_received) as distrib_received, max(bundle_info_new.bd_pic) as bd_pic"))
                                ->whereNull('bundle_info_new.is_output_cutting')
                                ->where('bundle_info_new.processing_fact', '2')
                                ->whereBetween('bundle_info_new.create_date', ['2021-06-01 00:00:00', $limit_date])
                                ->groupBy('bundle_info_new.style', 'art_desc_new.set_type', 'bundle_info_new.poreference', 'bundle_info_new.article', 'bundle_info_new.cut_num', 'bundle_info_new.start_num', 'bundle_info_new.end_num', 'bundle_info_new.qty', 'bundle_info_new.processing_fact', 'bundle_info_new.season', 'bundle_info_new.size')->get();
                                
            foreach($get_datas as $get_data) {
                $check_insert = OutputCutting::where('style', $get_data->style)
                ->where('set_type', $get_data->set_type)
                            ->where('poreference', $get_data->poreference)
                            ->where('article', $get_data->article)
                            ->where('cut_num', $get_data->cut_num)
                            ->where('start_num', $get_data->start_num)
                            ->where('end_num', $get_data->end_num)
                            ->where('qty', $get_data->qty)
                            ->where('processing_fact', $get_data->processing_fact)
                            ->where('season', $get_data->season)
                            ->where('size', $get_data->size)
                            ->get();
                            
                if($check_insert->count() < 1) {
                    $check_data = DB::connection('sds_live')
                                        ->table('art_desc_new')
                                        ->where('style', $get_data->style)
                                        ->where('set_type', $get_data->set_type)
                                        ->where('season', $get_data->season)
                                        ->get();

                    $check_data_ = DB::connection('sds_live')
                                        ->table('bundle_info_new')
                                        ->join('art_desc_new', function ($join) {
                                            $join->on('art_desc_new.style', '=', 'bundle_info_new.style')
                                                    ->on('art_desc_new.season', '=', 'bundle_info_new.season')
                                                    ->on('art_desc_new.part_name', '=', 'bundle_info_new.part_name')
                                                    ->on('art_desc_new.part_num', '=', 'bundle_info_new.part_num');
                                        })
                                        ->select('bundle_info_new.*')
                                        ->whereNotNull('bundle_info_new.distrib_received')
                                        ->where('bundle_info_new.distrib_received', '<', $limit_date)
                                        ->where('bundle_info_new.style', $get_data->style)
                                        ->where('art_desc_new.set_type', $get_data->set_type)
                                        ->where('bundle_info_new.season', $get_data->season)
                                        ->where('bundle_info_new.poreference', $get_data->poreference)
                                        ->where('bundle_info_new.article', $get_data->article)
                                        ->where('bundle_info_new.cut_num', $get_data->cut_num)
                                        ->where('bundle_info_new.size', $get_data->size)
                                        ->where('bundle_info_new.start_num', $get_data->start_num)
                                        ->where('bundle_info_new.end_num', $get_data->end_num)
                                        ->where('bundle_info_new.qty', $get_data->qty)
                                        ->where('bundle_info_new.processing_fact', $get_data->processing_fact)
                                        ->get();
                                    
                                    
                    if($check_data->count() == $check_data_->count()) {
                        $insert_data = OutputCutting::FirstOrCreate([
                            'style' => $get_data->style,
                            'set_type' => $get_data->set_type,
                            'poreference' => $get_data->poreference,
                            'article' => $get_data->article,
                            'cut_num' => $get_data->cut_num,
                            'start_num' => $get_data->start_num,
                            'end_num' => $get_data->end_num,
                            'qty' => $get_data->qty,
                            'deleted_at' => null,
                            'processing_fact' => $get_data->processing_fact,
                            'season' => $get_data->season,
                            'size' => $get_data->size,
                            'user_id' => $get_data->bd_pic,
                            'last_scan_at' => $get_data->distrib_received,
                        ]);

                        $get_barcode_ids = DB::connection('sds_live')
                                    ->table('bundle_info_new')
                                    ->join('art_desc_new', function ($join) {
                                        $join->on('art_desc_new.style', '=', 'bundle_info_new.style')
                                                ->on('art_desc_new.season', '=', 'bundle_info_new.season')
                                                ->on('art_desc_new.part_num', '=', 'bundle_info_new.part_num')
                                                ->on('art_desc_new.part_name', '=', 'bundle_info_new.part_name');
                                    })
                                    ->where('art_desc_new.set_type', '=', $get_data->set_type)
                                    ->where('bundle_info_new.style', $get_data->style)
                                    ->where('bundle_info_new.season', $get_data->season)
                                    ->where('bundle_info_new.article', $get_data->article)
                                    ->where('bundle_info_new.poreference', $get_data->poreference)
                                    ->where('bundle_info_new.size', $get_data->size)
                                    ->where('bundle_info_new.cut_num', $get_data->cut_num)
                                    ->where('bundle_info_new.start_num', $get_data->start_num)
                                    ->where('bundle_info_new.end_num', $get_data->end_num)
                                    ->where('bundle_info_new.qty', $get_data->qty)
                                    ->where('bundle_info_new.processing_fact', '2')->pluck('barcode_id')->toArray();

                        DB::connection('sds_live')
                            ->table('bundle_info_new')
                            ->whereIn('barcode_id', $get_barcode_ids)
                            ->update([
                                'is_output_cutting' => 't'
                            ]);
                    }
                }
            }
            
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
