<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use App\Models\Distribusi\ComponentReady;

class ComponentReadyController extends Controller
{
    static function outputSync()
    {
        // get time
        $from = Carbon::now()->subMinutes(20);
        $to = Carbon::now()->addMinutes(10);

        // start datebase excecution
        try 
        {
            DB::beginTransaction();

            $get_data_supplys = DB::connection('sds_live')
                                ->table('bundle_info_new')
                                ->join('art_desc_new', function ($join) {
                                    $join->on('art_desc_new.style', '=', 'bundle_info_new.style')
                                            ->on('art_desc_new.season', '=', 'bundle_info_new.season')
                                            ->on('art_desc_new.part_name', '=', 'bundle_info_new.part_name')
                                            ->on('art_desc_new.part_num', '=', 'bundle_info_new.part_num');
                                })
                                ->select(DB::raw("bundle_info_new.style, art_desc_new.set_type, bundle_info_new.poreference, bundle_info_new.article, bundle_info_new.cut_num, bundle_info_new.start_num, bundle_info_new.end_num, bundle_info_new.qty, bundle_info_new.processing_fact, bundle_info_new.season, bundle_info_new.size"))
                                ->whereNotNull('bundle_info_new.supply')
                                ->whereBetween('bundle_info_new.supply', [$from, $to])
                                ->where('bundle_info_new.processing_fact', '2')
                                ->groupBy('bundle_info_new.style', 'art_desc_new.set_type', 'bundle_info_new.poreference', 'bundle_info_new.article', 'bundle_info_new.cut_num', 'bundle_info_new.start_num', 'bundle_info_new.end_num', 'bundle_info_new.qty', 'bundle_info_new.processing_fact', 'bundle_info_new.season', 'bundle_info_new.size')->get();

            foreach($get_data_supplys as $get_data_supply) {
                $delete_data = ComponentReady::where('style', $get_data_supply->style)->where('set_type', $get_data_supply->set_type)->where('poreference', $get_data_supply->poreference)->where('article', $get_data_supply->article)->where('cut_num', $get_data_supply->cut_num)->where('start_num', $get_data_supply->start_num)->where('end_num', $get_data_supply->end_num)->where('qty', $get_data_supply->qty)->where('processing_fact', $get_data_supply->processing_fact)->where('season', $get_data_supply->season)->where('size', $get_data_supply->size)->delete();
            }

            $get_datas = DB::connection('sds_live')
                                ->table('bundle_info_new')
                                ->join('art_desc_new', function ($join) {
                                    $join->on('art_desc_new.style', '=', 'bundle_info_new.style')
                                            ->on('art_desc_new.season', '=', 'bundle_info_new.season')
                                            ->on('art_desc_new.part_name', '=', 'bundle_info_new.part_name')
                                            ->on('art_desc_new.part_num', '=', 'bundle_info_new.part_num');
                                })
                                ->select(DB::raw("bundle_info_new.style, art_desc_new.set_type, bundle_info_new.poreference, bundle_info_new.article, bundle_info_new.cut_num, bundle_info_new.start_num, bundle_info_new.end_num, bundle_info_new.qty, bundle_info_new.processing_fact, bundle_info_new.season, bundle_info_new.size, max(bundle_info_new.setting) as setting, location, count(bundle_info_new.poreference) as component_count"))
                                ->whereNotNull('bundle_info_new.setting')
                                ->whereNull('bundle_info_new.supply')
                                ->whereBetween('bundle_info_new.setting', [$from, $to])
                                ->where('bundle_info_new.processing_fact', '2')
                                ->groupBy('bundle_info_new.style', 'art_desc_new.set_type', 'bundle_info_new.poreference', 'bundle_info_new.article', 'bundle_info_new.cut_num', 'bundle_info_new.start_num', 'bundle_info_new.end_num', 'bundle_info_new.qty', 'bundle_info_new.processing_fact', 'bundle_info_new.season', 'bundle_info_new.size', 'bundle_info_new.location')->get();

            foreach($get_datas as $get_data) {
                $check_data = DB::connection('sds_live')
                                    ->table('art_desc_new')
                                    ->where('style', $get_data->style)
                                    ->where('set_type', $get_data->set_type)
                                    ->where('season', $get_data->season)
                                    ->get();

                if($get_data->set_type == '0') {
                    $style = $get_data->style;
                } else {
                    $style = $get_data->style.'-'.substr($get_data->set_type, 0, 3);
                }

                $smv_check = DB::connection('line_live')->table('gsdsmv_view')->where('style', $style)->whereNotNull('gsd_smv')->get();

                if($smv_check->count() > 0) {
                    $gsd_smv = $smv_check->first()->gsd_smv;
                    $total_smv = $gsd_smv * $get_data->qty;
                } else {
                    $gsd_smv = 0;
                    $total_smv = 0;
                }
                                    
                $insert_data = ComponentReady::FirstOrCreate([
                    'style' => $get_data->style,
                    'set_type' => $get_data->set_type,
                    'poreference' => $get_data->poreference,
                    'article' => $get_data->article,
                    'cut_num' => $get_data->cut_num,
                    'start_num' => $get_data->start_num,
                    'end_num' => $get_data->end_num,
                    'qty' => $get_data->qty,
                    'deleted_at' => null,
                    'processing_fact' => $get_data->processing_fact,
                    'season' => $get_data->season,
                    'size' => $get_data->size,
                    'location' => $get_data->location,
                ]);
                
                $insert_data->gsd_smv = $gsd_smv;
                $insert_data->total_smv = $total_smv;
                $insert_data->component_complete = $check_data->count();
                $insert_data->component_ready = $get_data->component_count;
                $insert_data->last_scan_at = $get_data->setting;
                if($check_data->count() == $get_data->component_count) {
                    $insert_data->is_ready = true;
                } else {
                    $insert_data->is_ready = false;
                }
                $insert_data->save();
            }
            
            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
