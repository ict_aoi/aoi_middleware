<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Carbon\Carbon;
use Symfony\Component\Debug\ErrorHandler;

use App\Http\Controllers\API\ApiController;

class IntegrationController extends Controller
{
    //
    static function OrderReceipt()
    {
        $resp = '';
        $data_fg = DB::connection('fgms_aoi')
                        ->table('v_integration_in_new_2')
                        //->where('po_number_rasio', '0129262203') //tes
                        //->where('barcode_id', '21122401373600001') //test
                        ->where('is_integrate', false)
                        ->get();


        if (count($data_fg) > 0) {
            //do database action
            try {
                DB::beginTransaction();
                    foreach ($data_fg as $key => $val) {
                        //get mo erp
                        $data_erp = DB::connection('erp_live')
                                    ->table('bma_mo_by_order_product_v1')
                                    ->where('poreference', $val->po_number_rasio)
                                    ->where('upc', $val->style)
                                    ->where('kst_articleno', $val->buyer_item)
                                    //->where('manufacturing_size', $val->manufacturing_size)
                                    ->where(function($q) use($val){
                                        $q->Where('size', $val->manufacturing_size);
                                        $q->orWhere('size', $val->customer_size);
                                    })
                                    ->first();
                        //
                        if (!$data_erp) {
                            continue;
                        }

                        
                        $cekifexist = DB::connection('fgms_aoi')
                                        ->table('package')
                                        ->where('barcode_id', $val->barcode_id)
                                        ->where('is_integrate', false)
                                        ->exists();

                        if (!$cekifexist) {
                            continue;
                        }
            
                        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:_0="http://idempiere.org/ADInterface/1_0">
                        <soapenv:Header/>
                            <soapenv:Body>
                                <_0:compositeOperation>
                                    <_0:CompositeRequest>
                                        <_0:ADLoginRequest>
                                            <_0:user>FGMS</_0:user>
                                            <_0:pass>FGMS100%</_0:pass>
                                            <_0:lang>EN</_0:lang>
                                            <_0:RoleID>1000163</_0:RoleID>
                                            <_0:ClientID>'.$data_erp->ad_client_id.'</_0:ClientID> <!-- From MO -->
                                            <_0:OrgID>'.$data_erp->ad_org_id.'</_0:OrgID> <!-- From MO -->
                                            <_0:WarehouseID>'.$val->locator_sewing.'</_0:WarehouseID> <!-- From MO -->
                                            <_0:stage>0</_0:stage>
                                        </_0:ADLoginRequest>
                                        <_0:serviceType>composite</_0:serviceType>
                                        <!-- Item 1 -->
                                        <_0:operations>
                                            <!--Cost Collector Group -->
                                            <_0:operation preCommit="false" postCommit="false">
                                                <_0:TargetPort>createData</_0:TargetPort>
                                                <_0:ModelCRUD>
                                                    <_0:serviceType>create_costcollectorgroup</_0:serviceType>
                                                    <_0:DataRow>
                                                        <_0:field column="AD_Org_ID">
                                                            <_0:val>'.$data_erp->ad_org_id.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <_0:field column="C_DocTypeTarget_ID" lval="Manufacturing Cost Collector Group" />
                                                        <_0:field column="C_DocType_ID" lval="Manufacturing Cost Collector Group" />
                                                        <_0:field column="DateAcct">
                                                            <_0:val>'.Carbon::now().'</_0:val> <!-- Processing Date -->
                                                        </_0:field>
                                                        <_0:field column="PP_Order_ID"> <!-- from MO -->
                                                            <_0:val>'.$data_erp->pp_order_id.'</_0:val>
                                                        </_0:field>
                                                    </_0:DataRow>
                                                </_0:ModelCRUD>
                                            </_0:operation>
                                            <!--Cost Collector Material Receipt-->
                                            <_0:operation preCommit="false" postCommit="false">
                                                <_0:TargetPort>createData</_0:TargetPort>
                                                <_0:ModelCRUD>
                                                    <_0:serviceType>create_costcollector</_0:serviceType>
                                                    <_0:DataRow>
                                                        <_0:field column="PP_Cost_CollectorGroup_ID">
                                                            <_0:val>@PP_Cost_CollectorGroup.PP_Cost_CollectorGroup_ID</_0:val>
                                                        </_0:field>
                                                        <_0:field column="AD_Org_ID">
                                                            <_0:val>@PP_Cost_CollectorGroup.AD_Org_ID</_0:val>
                                                        </_0:field>
                                                        <_0:field column="MovementDate">
                                                            <_0:val>@PP_Cost_CollectorGroup.DateAcct</_0:val>
                                                        </_0:field>
                                                        <_0:field column="DateAcct">
                                                            <_0:val>@PP_Cost_CollectorGroup.DateAcct</_0:val>
                                                        </_0:field>
                                                        <_0:field column="PP_Order_ID">
                                                            <_0:val>'.$data_erp->pp_order_id.'</_0:val>
                                                        </_0:field>
                                                        <!-- Sewing -->
                                                        <_0:field column="C_DocTypeTarget_ID" lval="Manufacturing Cost Collector"/>
                                                        <_0:field column="C_DocType_ID" lval="Manufacturing Cost Collector"/>
                                                        <!-- AOI Plant -->
                                                        <_0:field column="S_Resource_ID">
                                                            <_0:val>'.$data_erp->s_resource_id.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <!-- Cost Collector Type : Material Receipt -->
                                                        <_0:field column="CostCollectorType">
                                                            <_0:val>100</_0:val> <!-- DONT CHANGE -->
                                                        </_0:field>
                                                        <!-- Accessories AOI 1 -->
                                                        <_0:field column="M_Warehouse_ID">
                                                            <_0:val>'.$val->locator_sewing.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <!-- Inventory Acc AOI 1 -->
                                                        <_0:field column="M_Locator_ID">
                                                            <_0:val>'.$val->locator_inventory.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <_0:field column="M_Product_ID">
                                                            <_0:val>'.$data_erp->m_product_id.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <_0:field column="MovementQty">
                                                            <_0:val>'.$val->inner_pack.'</_0:val> <!-- Quantity Receipt -->
                                                        </_0:field>
                                                        <!-- FGMS ID 17 Character -->
                                                        <_0:field column="WebService_UU">
                                                            <_0:val>'.$val->barcode_id.'</_0:val>
                                                        </_0:field>
                                                    </_0:DataRow>
                                                </_0:ModelCRUD>
                                            </_0:operation>
                                            <!--DocAction-->
                                            <_0:operation preCommit="false" postCommit="true">
                                                <_0:TargetPort>setDocAction</_0:TargetPort>
                                                <_0:ModelSetDocAction>
                                                    <_0:serviceType>docaction_costcollectorgroup</_0:serviceType>
                                                    <_0:tableName>PP_Cost_CollectorGroup</_0:tableName>
                                                    <_0:recordIDVariable>@PP_Cost_CollectorGroup.PP_Cost_CollectorGroup_ID</_0:recordIDVariable>
                                                    <_0:docAction>CO</_0:docAction>
                                                </_0:ModelSetDocAction>
                                            </_0:operation>
                                            <!--DocAction-->
                                        </_0:operations>
                                    </_0:CompositeRequest>
                                </_0:compositeOperation>
                            </soapenv:Body>
                        </soapenv:Envelope>';
                        //return response($xml)
                        //            ->header('Content-Type', 'text/xml');
                
                        $response = ApiController::callAPI("POST", ApiController::$url.'/compositeInterface', $xml);
                
                        // SimpleXML seems to have problems with the colon ":" in the <xxx:yyy> response tags, so take them out
                        $xml_response = preg_replace("/(<\/?)(\w+):([^>]*>)/", '$1$2$3', $response);
                        $xml_response = simplexml_load_string($xml_response);
                        $json = json_encode($xml_response);
                        $responseArray = json_decode($json, true);
                        
                        //sukses
                        if (isset($responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['RecordID'])) {
        
                            if (!isset($responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['IsRolledBack'])) {
                                
                                DB::connection('fgms_aoi')->table('package')->where('barcode_id', $val->barcode_id)
                                                    ->update([
                                                        'is_integrate' => true,
                                                        'integration_date' => Carbon::now(),
                                                        'updated_at' => Carbon::now(),
                                                        'remark_integration' => $responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['RecordID'],
                                                        'is_reverse' => false,
                                                        'reverse_date' => null,
                                                        'remark_reverse' => null
                                                    ]);
                            }else {
                                DB::connection('fgms_aoi')->table('package')->where('barcode_id', $val->barcode_id)
                                                ->update([
                                                    'updated_at' => Carbon::now(),
                                                    'remark_integration' => $json
                                                ]);
                            }
                        }else {
                            DB::connection('fgms_aoi')->table('package')->where('barcode_id', $val->barcode_id)
                                                ->update([
                                                    'updated_at' => Carbon::now(),
                                                    'remark_integration' => $json
                                                ]);
                        }
                        //                array:1 [▼
                        //  "@attributes" => array:1 [▼
                        //    "RecordID" => "17519254"
                        //  ]
                        //]

                        $resp .= $json;
                    }
                DB::commit();
            }
            catch (Exception $ex) {
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
            finally {
                //REFRESH MATERIALIZED VIEW
                //DB::connection('fgms_aoi')->select('REFRESH MATERIALIZED VIEW v_integration_in_new_2');

                ////////REFRESH MATERIALIZED VIEW
                //DB::connection('fgms_aoi')->select('REFRESH MATERIALIZED VIEW v_integration_reverse_receipt');
            }

        }

        //////REFRESH MATERIALIZED VIEW
        DB::connection('fgms_aoi')->select('REFRESH MATERIALIZED VIEW v_integration_reverse_receipt');

        return response()->json($resp, 200);

    }


    static function ReverseReceipt()
    {
        $resp = '';

        $data_fg = DB::connection('fgms_aoi')->table('v_integration_reverse_receipt')
                        //->where('barcode_id', '21122401373600007') //test
                        ->get();

        if (count($data_fg) > 0) {
            //do database action
            try {
                DB::beginTransaction();

                    foreach ($data_fg as $key => $val) {
                        //get mo erp
                        $data_erp = DB::connection('erp_live')
                                    ->table('bma_mo_by_order_product_v1')
                                    ->where('poreference', $val->po_number_rasio)
                                    ->where('upc', $val->style)
                                    ->where('kst_articleno', $val->buyer_item)
                                    //->where('manufacturing_size', $val->manufacturing_size)
                                    ->where(function($q) use($val){
                                        $q->Where('size', $val->manufacturing_size);
                                        $q->orWhere('size', $val->customer_size);
                                    })
                                    ->first();
        
                        //get atributesetinstant erp
                        $data_cost = DB::connection('erp_live')
                                    ->table('pp_cost_collector')
                                    ->where('webservice_uu', $val->barcode_id)
                                    ->first();
                        //
                        if (!$data_erp) {
                            continue;
                        }

                        $cekifexist = DB::connection('fgms_aoi')
                                        ->table('package')
                                        ->where('barcode_id', $val->barcode_id)
                                        ->where('is_integrate', true)
                                        ->exists();

                        if (!$cekifexist) {
                            continue;
                        }
            
                        $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:_0="http://idempiere.org/ADInterface/1_0">
                            <soapenv:Header/>
                            <soapenv:Body>
                                <_0:compositeOperation>
                                    <_0:CompositeRequest>
                                        <_0:ADLoginRequest>
                                            <_0:user>FGMS</_0:user>
                                            <_0:pass>FGMS100%</_0:pass>
                                            <_0:lang>EN</_0:lang>
                                            <_0:RoleID>1000163</_0:RoleID>
                                            <_0:ClientID>'.$data_erp->ad_client_id.'</_0:ClientID> <!-- From MO -->
                                            <_0:OrgID>'.$data_erp->ad_org_id.'</_0:OrgID> <!-- From MO -->
                                            <_0:WarehouseID>'.$val->locator_sewing.'</_0:WarehouseID> <!-- From MO -->
                                            <_0:stage>0</_0:stage>
                                        </_0:ADLoginRequest>
                                        <_0:serviceType>composite</_0:serviceType>
                                        <!-- Item 1 -->
                                        <_0:operations>
                                            <!--Cost Collector Group -->
                                            <_0:operation preCommit="false" postCommit="false">
                                                <_0:TargetPort>createData</_0:TargetPort>
                                                <_0:ModelCRUD>
                                                    <_0:serviceType>create_costcollectorgroup</_0:serviceType>
                                                    <_0:DataRow>
                                                        <_0:field column="AD_Org_ID">
                                                            <_0:val>'.$data_erp->ad_org_id.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <_0:field column="C_DocTypeTarget_ID" lval="Manufacturing Cost Collector Group" />
                                                        <_0:field column="C_DocType_ID" lval="Manufacturing Cost Collector Group" />
                                                        <_0:field column="DateAcct">
                                                            <_0:val>'.Carbon::now().'</_0:val> <!-- Processing Date -->
                                                        </_0:field>
                                                        <_0:field column="PP_Order_ID"> <!-- From MO -->
                                                            <_0:val>'.$data_erp->pp_order_id.'</_0:val>
                                                        </_0:field>
                                                    </_0:DataRow>
                                                </_0:ModelCRUD>
                                            </_0:operation>
                                            <!--Cost Collector Material Receipt-->
                                            <_0:operation preCommit="false" postCommit="false">
                                                <_0:TargetPort>createData</_0:TargetPort>
                                                <_0:ModelCRUD>
                                                    <_0:serviceType>create_costcollector</_0:serviceType>
                                                    <_0:DataRow>
                                                        <_0:field column="PP_Cost_CollectorGroup_ID">
                                                            <_0:val>@PP_Cost_CollectorGroup.PP_Cost_CollectorGroup_ID</_0:val>
                                                        </_0:field>
                                                        <_0:field column="AD_Org_ID">
                                                            <_0:val>@PP_Cost_CollectorGroup.AD_Org_ID</_0:val>
                                                        </_0:field>
                                                        <_0:field column="MovementDate">
                                                            <_0:val>@PP_Cost_CollectorGroup.DateAcct</_0:val>
                                                        </_0:field>
                                                        <_0:field column="DateAcct">
                                                            <_0:val>@PP_Cost_CollectorGroup.DateAcct</_0:val>
                                                        </_0:field>
                                                        <_0:field column="PP_Order_ID">
                                                            <_0:val>@PP_Cost_CollectorGroup.PP_Order_ID</_0:val>
                                                        </_0:field>
                                                        <!-- Sewing -->
                                                        <_0:field column="C_DocTypeTarget_ID" lval="Manufacturing Cost Collector"/>
                                                        <_0:field column="C_DocType_ID" lval="Manufacturing Cost Collector"/>
                                                        <!-- AOI Plant -->
                                                        <_0:field column="S_Resource_ID">
                                                            <_0:val>'.$data_erp->s_resource_id.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <!-- Cost Collector Type : Material Receipt -->
                                                        <_0:field column="CostCollectorType">
                                                            <_0:val>100</_0:val> <!-- DONT CHANGE -->
                                                        </_0:field>
                                                        <!-- Accessories AOI 1 -->
                                                        <_0:field column="M_Warehouse_ID">
                                                            <_0:val>'.$val->locator_sewing.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <!-- Inventory Acc AOI 1 -->
                                                        <_0:field column="M_Locator_ID">
                                                            <_0:val>'.$val->locator_inventory.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <_0:field column="M_Product_ID">
                                                            <_0:val>'.$data_erp->m_product_id.'</_0:val> <!-- From MO -->
                                                        </_0:field>
                                                        <_0:field column="MovementQty">
                                                            <_0:val>-'.$val->inner_pack.'</_0:val> <!-- Quantity Reverse -->
                                                        </_0:field>
                                                        <_0:field column="M_AttributeSetInstance_ID">
                                                            <_0:val>'.$data_cost->m_attributesetinstance_id.'</_0:val>
                                                        </_0:field>
                                                    </_0:DataRow>
                                                </_0:ModelCRUD>
                                            </_0:operation>
                                            <!--DocAction-->
                                            <_0:operation preCommit="false" postCommit="true">
                                                <_0:TargetPort>setDocAction</_0:TargetPort>
                                                <_0:ModelSetDocAction>
                                                    <_0:serviceType>docaction_costcollectorgroup</_0:serviceType>
                                                    <_0:tableName>PP_Cost_CollectorGroup</_0:tableName>
                                                    <_0:recordIDVariable>@PP_Cost_CollectorGroup.PP_Cost_CollectorGroup_ID</_0:recordIDVariable>
                                                    <_0:docAction>CO</_0:docAction>
                                                </_0:ModelSetDocAction>
                                            </_0:operation>
                                            <!--DocAction-->
                                        </_0:operations>
                                    </_0:CompositeRequest>
                                </_0:compositeOperation>
                            </soapenv:Body>
                        </soapenv:Envelope>';
                        //return response($xml)
                        //            ->header('Content-Type', 'text/xml');
                
                        $response = ApiController::callAPI("POST", ApiController::$url.'/compositeInterface', $xml);
                
                        // SimpleXML seems to have problems with the colon ":" in the <xxx:yyy> response tags, so take them out
                        $xml_response = preg_replace("/(<\/?)(\w+):([^>]*>)/", '$1$2$3', $response);
                        $xml_response = simplexml_load_string($xml_response);
                        $json = json_encode($xml_response);
                        $responseArray = json_decode($json, true);
        
                        //sukses
                        if (isset($responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['RecordID'])) {
        
                            if (!isset($responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['IsRolledBack'])) {
                                
                                DB::connection('fgms_aoi')->table('history_package')->where('barcode_id', $val->barcode_id)
                                                    ->update([
                                                        'is_reverse' => true,
                                                        'reverse_date' => Carbon::now(),
                                                        'updated_at' => Carbon::now(),
                                                        'remark_reverse' => $responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['RecordID']
                                                    ]);
        
                                ///
                                DB::connection('fgms_aoi')->table('package')->where('barcode_id', $val->barcode_id)
                                                    ->update([
                                                        'is_reverse' => true,
                                                        'reverse_date' => Carbon::now(),
                                                        'updated_at' => Carbon::now(),
                                                        'remark_reverse' => $responseArray['soapBody']['ns1compositeOperationResponse']['CompositeResponses']['CompositeResponse']['StandardResponse'][1]['@attributes']['RecordID'],
                                                        'is_integrate' => false,
                                                        'integration_date' => null,
                                                    ]);
        
                            }else {
                                DB::connection('fgms_aoi')->table('history_package')->where('barcode_id', $val->barcode_id)
                                                ->update([
                                                    'updated_at' => Carbon::now(),
                                                    'remark_reverse' => $json
                                                ]);
                                //
                                DB::connection('fgms_aoi')->table('package')->where('barcode_id', $val->barcode_id)
                                                ->update([
                                                    'updated_at' => Carbon::now(),
                                                    'remark_reverse' => $json
                                                ]);
                                //
                            }
                        }else {
                            DB::connection('fgms_aoi')->table('history_package')->where('barcode_id', $val->barcode_id)
                                                ->update([
                                                    'updated_at' => Carbon::now(),
                                                    'remark_reverse' => $json
                                                ]);
                            //
                            DB::connection('fgms_aoi')->table('package')->where('barcode_id', $val->barcode_id)
                                                ->update([
                                                    'updated_at' => Carbon::now(),
                                                    'remark_reverse' => $json
                                                ]);
                            //
                        }

                        $resp .= $json;
                    
                    }
                    
                DB::commit();
            }
            catch (Exception $ex) {
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
            finally {
                ////REFRESH MATERIALIZED VIEW
                //DB::connection('fgms_aoi')->select('REFRESH MATERIALIZED VIEW v_integration_in_new_2');

                //////REFRESH MATERIALIZED VIEW
                //DB::connection('fgms_aoi')->select('REFRESH MATERIALIZED VIEW v_integration_reverse_receipt');
            }

        }

        //REFRESH MATERIALIZED VIEW
        DB::connection('fgms_aoi')->select('REFRESH MATERIALIZED VIEW v_integration_in_new_2');

        return response()->json($resp, 200);

    }
}
