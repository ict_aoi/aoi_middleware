<?php

namespace App\Http\Controllers\Integration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class FinishGoodsController extends Controller
{
    //
    static function StockBalanceInsert()
    {
        $data_stock_balance = DB::connection('fgms_aoi')
                                ->table('package')
                                ->select(
                                    'package.barcode_id',
                                    'package_detail.inner_pack',
                                    'package_detail.customer_size',
                                    'package_detail.length',
                                    'package_detail.width',
                                    'package_detail.height',
                                    'package_detail.unit_2',
                                    'po_summary.po_number',
                                    'po_summary.upc',
                                    'package.created_at',
                                    'package.updated_at',
                                    'package_movements.barcode_package',
                                    'package_movements.created_at AS checkin',
                                    'locators.code',
                                    'package_detail.manufacturing_size',
                                    'package.current_department',
                                    'package.current_status',
                                    'package_detail.factory_id',
                                    'line.description'
                                )
                                ->join('package_detail', 'package_detail.scan_id','=','package.scan_id')
                                ->join('po_summary', function($join) {
                                    $join->on('package_detail.po_number','=','po_summary.po_number');
                                    $join->on('package_detail.plan_ref_number','=','po_summary.plan_ref_number');
                                })
                                ->leftjoin('package_movements', 'package_movements.barcode_package','=','package.barcode_id')
                                ->leftjoin('locators','locators.barcode','=','package_movements.barcode_locator')
                                ->leftjoin('line','line.id','=','package_movements.line_id')
                                ->where(function($query) {
                                    $query->where(function($query) {
                                        $query->where('current_department', 'inventory')
                                            ->where('current_status', 'onprogress');
                                    })
                                    ->orWhere(function($query) {
                                        $query->where('current_department', 'inventory')
                                        ->where('current_status', 'completed');
                                    })
                                    ->orWhere(function($query) {
                                        $query->where('current_department', 'qcinspect')
                                        ->where('current_status', 'onprogress');
                                    })
                                    ->orWhere(function($query) {
                                        $query->where('current_department', 'qcinspect')
                                        ->where('current_status', 'completed');
                                    })
                                    ->orWhere(function($query) {
                                        $query->where('current_department', 'shipping')
                                        ->where('current_status', 'onprogress');
                                    })
                                    ->orWhere(function($query) {
                                        $query->where('current_department', 'sewing')
                                        ->where('current_status', 'completed');
                                    });

                                })
                                // ->where('package_detail.factory_id', $factory_id)
                                ->where('is_canceled',false)
                                ->whereNull('package.deleted_at')
                                ->whereNull('package_detail.deleted_at')
                                ->whereNull('po_summary.deleted_at')
                                ->whereNull('package_movements.deleted_at')
                                ->groupBy(
                                    'package.barcode_id',
                                    'package_detail.inner_pack',
                                    'package_detail.customer_size',
                                    'package_detail.length',
                                    'package_detail.width',
                                    'package_detail.height',
                                    'package_detail.unit_2',
                                    'po_summary.po_number',
                                    'po_summary.upc',
                                    'locators.code',
                                    'package.created_at',
                                    'package.updated_at',
                                    'package_movements.created_at',
                                    'package_movements.barcode_package',
                                    'package_detail.manufacturing_size',
                                    'package.current_department',
                                    'package.current_status',
                                    'package_detail.factory_id',
                                    'line.description'
                                    )
                                ->orderBy('po_summary.po_number','asc')
                                ->orderBy('package.created_at','asc')
                                ->get();
    try {
        DB::begintransaction();
        
        foreach ($data_stock_balance as $key => $val) {
            // cek data end balance
            $barcode_id = $val->barcode_id;

            $range = array(
                'from' => date_format(date_create(Carbon::now()), 'Y-m-d 00:00:00'),
                'to' => date_format(date_create(Carbon::now()), 'Y-m-d 23:59:59')
            );

            $check_exist = DB::connection('fgms_aoi')
                            ->table('end_balance')
                            ->where('barcode_id', $barcode_id)
                            ->where(function($query) use ($range) {
                                $query->whereBetween('date_balance', [$range['from'], $range['to']]);
                            })
                            ->exists();
            
            if (!$check_exist) {

                DB::connection('fgms_aoi')
                    ->table('end_balance')
                    ->insert([
                            'barcode_id' => $val->barcode_id,
                            'upc' => $val->upc,
                            'po_number' => $val->po_number,
                            'customer_size' => $val->customer_size,
                            'manufacturing_size' => $val->manufacturing_size,
                            'inner_pack' => $val->inner_pack,
                            'length' => $val->length,
                            'width' => $val->width,
                            'height' => $val->height,
                            'unit_2' => $val->unit_2,
                            'created_at' => $val->created_at,
                            'updated_at' => $val->updated_at,
                            'checkin' => $val->checkin,
                            'code' => $val->code,
                            'current_department' => $val->current_department,
                            'current_status' => $val->current_status,
                            'factory_id' => $val->factory_id,
                            'date_balance' => Carbon::now(),
                            'description' => $val->description
                    ]);
            }
        }
        
        DB::commit();

    } catch (Exception $ex) {
        DB::rollback();
        $message = $ex->getMessage();
        ErrorHandler::db($message);
    }

        return response()->json('success', 200);
    }

    static function sync_cancel(){
        try {
            $getCancel = DB::connection('fgms_aoi')
                                ->table('po_summary_sync')
                                ->select('po_summary_sync.po_number','po_summary.plan_ref_number')
                                ->join('po_summary','po_summary.po_number','=','po_summary_sync.po_number')
                                ->where(function($query){
                                    $query->Where('po_summary_sync.is_cancel_order',true);
                                    $query->orWhere('po_summary_sync.is_re_route',true);
                                })
                                ->where('po_summary.is_cancel_order',false)
                                ->whereNull('po_summary.deleted_at')
                                ->get();

            foreach ($getCancel as $gc) {
                DB::connection('fgms_aoi')
                    ->table('po_summary')
                    ->where('po_number',$gc->po_number)
                    ->where('plan_ref_number',$gc->plan_ref_number)
                    ->wherenull('deleted_at')
                    ->update(['is_cancel_order'=>true,'updated_at'=>Carbon::now()]);

                DB::connection('fgms_aoi')
                    ->table('package_detail')
                    ->where('po_number',$gc->po_number)
                    ->where('plan_ref_number',$gc->plan_ref_number)
                    ->wherenull('deleted_at')
                    ->update(['is_cancel_order'=>true,'updated_at'=>Carbon::now()]);
            }


        } catch (Exception $er) {
            DB::rollback();
            $message = $er->getMessage();
            ErrorHandler::db($message);
        }
        
    }
}
