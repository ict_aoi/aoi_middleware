<?php namespace App\Http\Controllers\Integration;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Integration\Finger;
use App\Models\Integration\FingerAOI2;
use App\Models\Integration\FingerBBIS;

use App\Models\Integration\Absensi;
use App\Models\Integration\AbsensiBBIS;

use App\Models\Integration\Employee;
use App\Models\Integration\EmployeeBBIS;

class AttendanceController extends Controller
{
    static function daily_sync(){
        $data = Finger::orderby('finger_date','desc')->get();
        
        //return response()->json($data);
        foreach ($data as $key => $value) {
            $nik = $value->nik;
            $finger_date = $value->finger_date;
            $_finger_date = $value->_finger_date;
            $finger_status = strtolower($value->finger_status);
            $finger_id = $value->finger_id;
            
            //$is_employee_exists = Employee::where('nik',$nik)->exists();
            $is_employee_exists = Employee::where('nik',$nik)->exists();

            if($is_employee_exists){
                $check_exists = Absensi::where([
                    [db::raw('trim(nik)'),trim($nik)],
                    [db::raw('trim("action")'),trim($finger_status)],
                    [db::raw("to_char(\"date\",'yyyy-mm-dd')"),$_finger_date],
                ])
                ->exists();
               
                if(!$check_exists){
                    Absensi::firstOrCreate([
                        'nik' => $nik,
                        'status' => 'normal',
                        'action' => $finger_status,
                        'created_time' => $finger_date,
                        'date' => $_finger_date,
                        'finger_id' => $finger_id,
                        'created_by' => 'finger',
                    ]);
    
                    DB::connection('finger_aoi_1')
                    ->table('att_log')
                    ->where('att_id',$finger_id)
                    ->update([
                        'work_io' => '1', 
                        'state' => 'OK', 
                    ]);
                }else{
                    $is_exists = Absensi::where([
                        [db::raw('trim(nik)'),trim($nik)],
                        [db::raw('trim("action")'),trim($finger_status)],
                        [db::raw("to_char(\"date\",'yyyy-mm-dd')"),$_finger_date],
                    ])
                    ->first();

                    if($finger_status == 'in'){
    
                        if($finger_date < $is_exists->created_time){
                            $_finger_id = $is_exists->finger_id;
                            $is_exists->finger_id = $finger_id;
                            $is_exists->created_time = $finger_date;
                            $is_exists->date = $_finger_date;
                            $is_exists->save();
    
                            DB::connection('finger_aoi_1')
                            ->table('att_log')
                            ->where('att_id',$finger_id)
                            ->update([
                                'work_io' => '1', 
                                'state' => 'OK', 
                            ]);
                        }
    
                        DB::connection('finger_aoi_1')
                        ->table('att_log')
                        ->where([
                            ['att_id','!=',$is_exists->finger_id],
                            ['io_mode','0'],
                            ['pin',$nik],
                            [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                        ])
                        ->update([
                            'work_io' => '1', 
                            'state' => 'NOT-OK, DATENGNYA DULUAN ID '.$is_exists->finger_id, 
                        ]);
                    }else if($finger_status == 'out'){
                        if($finger_date > $is_exists->attendance_date){
                            $_finger_id = $is_exists->finger_id;
                            $is_exists->finger_id = $finger_id;
                            $is_exists->created_time = $finger_date;
                            $is_exists->date = $_finger_date;
                            $is_exists->save();
    
                            DB::connection('finger_aoi_1')
                            ->table('att_log')
                            ->where('att_id',$finger_id)
                            ->update([
                                'work_io' => '1', 
                                'state' => 'OK', 
                            ]);
                        }
    
                        DB::connection('finger_aoi_1')
                        ->table('att_log')
                        ->where([
                            ['att_id','!=',$is_exists->finger_id],
                            ['io_mode','1'],
                            ['pin',$nik],
                            [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                        ])
                        ->update([
                            'work_io' => '1', 
                            'state' => 'NOT-OK, PULANGNYA LAMAAN ID '.$is_exists->finger_id, 
                        ]);
                    }
    
    
                }
            }else{
                DB::connection('finger_aoi_1')
                ->table('att_log')
                ->where([
                    ['att_id',$finger_id],
                    ['pin',$nik],
                    [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                ])
                ->update([
                    'work_io' => '1', 
                    'state' => 'NOT-OK, NIK INI TIDAK TERDAFTAR DI ABSENSI', 
                ]);
            }
            
        }
        return response()->json($data);
       
    }

    static function daily_sync_aoi_2(){
        $data = FingerAOI2::orderby('finger_date','desc')->get();
        
        //return response()->json($data);
        foreach ($data as $key => $value) {
            $nik = $value->nik;
            $finger_date = $value->finger_date;
            $_finger_date = $value->_finger_date;
            $finger_status = strtolower($value->finger_status);
            $finger_id = $value->finger_id;
            
            //$is_employee_exists = Employee::where('nik',$nik)->exists();
            $is_employee_exists = Employee::where('nik',$nik)->exists();

            if($is_employee_exists){
                $check_exists = Absensi::where([
                    [db::raw('trim(nik)'),trim($nik)],
                    [db::raw('trim("action")'),trim($finger_status)],
                    [db::raw("to_char(\"date\",'yyyy-mm-dd')"),$_finger_date],
                ])
                ->exists();
               
                if(!$check_exists){
                    Absensi::firstOrCreate([
                        'nik' => $nik,
                        'status' => 'normal',
                        'action' => $finger_status,
                        'created_time' => $finger_date,
                        'date' => $_finger_date,
                        'finger_id' => $finger_id,
                        'created_by' => 'finger',
                    ]);
    
                    DB::connection('finger_aoi_2')
                    ->table('att_log')
                    ->where('att_id',$finger_id)
                    ->update([
                        'work_io' => '1', 
                        'state' => 'OK', 
                    ]);
                }else{
                    $is_exists = Absensi::where([
                        [db::raw('trim(nik)'),trim($nik)],
                        [db::raw('trim("action")'),trim($finger_status)],
                        [db::raw("to_char(\"date\",'yyyy-mm-dd')"),$_finger_date],
                    ])
                    ->first();

                    if($finger_status == 'in'){
    
                        if($finger_date < $is_exists->created_time){
                            $_finger_id = $is_exists->finger_id;
                            $is_exists->finger_id = $finger_id;
                            $is_exists->created_time = $finger_date;
                            $is_exists->date = $_finger_date;
                            $is_exists->save();
    
                            DB::connection('finger_aoi_2')
                            ->table('att_log')
                            ->where('att_id',$finger_id)
                            ->update([
                                'work_io' => '1', 
                                'state' => 'OK', 
                            ]);
                        }
    
                        DB::connection('finger_aoi_2')
                        ->table('att_log')
                        ->where([
                            ['att_id','!=',$is_exists->finger_id],
                            ['io_mode','0'],
                            ['pin',$nik],
                            [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                        ])
                        ->update([
                            'work_io' => '1', 
                            'state' => 'NOT-OK, DATENGNYA DULUAN ID '.$is_exists->finger_id, 
                        ]);
                    }else if($finger_status == 'out'){
                        if($finger_date > $is_exists->created_time){
                            $_finger_id = $is_exists->finger_id;
                            $is_exists->finger_id = $finger_id;
                            $is_exists->created_time = $finger_date;
                            $is_exists->date = $_finger_date;
                            $is_exists->save();
    
                            DB::connection('finger_aoi_2')
                            ->table('att_log')
                            ->where('att_id',$finger_id)
                            ->update([
                                'work_io' => '1', 
                                'state' => 'OK', 
                            ]);
                        }
    
                        DB::connection('finger_aoi_2')
                        ->table('att_log')
                        ->where([
                            ['att_id','!=',$is_exists->finger_id],
                            ['io_mode','1'],
                            ['pin',$nik],
                            [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                        ])
                        ->update([
                            'work_io' => '1', 
                            'state' => 'NOT-OK, PULANGNYA LAMAAN ID '.$is_exists->finger_id, 
                        ]);
                    }
    
    
                }
            }else{
                DB::connection('finger_aoi_2')
                ->table('att_log')
                ->where([
                    ['att_id',$finger_id],
                    ['pin',$nik],
                    [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                ])
                ->update([
                    'work_io' => '1', 
                    'state' => 'NOT-OK, NIK INI TIDAK TERDAFTAR DI ABSENSI', 
                ]);
            }
            
        }
        return response()->json($data);
       
    }

    static function daily_sync_bbis()
    {
        $data = FingerBBIS::orderby('finger_date','desc')->get();
        
        //return response()->json($data);
        foreach ($data as $key => $value) 
        {
            $nik = $value->nik;
            $finger_date = $value->finger_date;
            $_finger_date = $value->_finger_date;
            $finger_status = strtolower($value->finger_status);
            $finger_id = $value->finger_id;
            
            //$is_employee_exists = Employee::where('nik',$nik)->exists();
            $is_employee_exists = EmployeeBBIS::where('nik',$nik)->exists();

            if($is_employee_exists)
            {
                $check_exists = AbsensiBBIS::where([
                    [db::raw('trim(nik)'),trim($nik)],
                    [db::raw('trim("action")'),trim($finger_status)],
                    [db::raw("to_char(\"date\",'yyyy-mm-dd')"),$_finger_date],
                ])
                ->exists();
               
                if(!$check_exists)
                {
                    AbsensiBBIS::firstOrCreate([
                        'nik' => $nik,
                        'status' => 'normal',
                        'action' => $finger_status,
                        'created_time' => $finger_date,
                        'date' => $_finger_date,
                        'finger_id' => $finger_id,
                        'created_by' => 'finger',
                    ]);
    
                    DB::connection('finger_bbis')
                    ->table('att_log')
                    ->where('att_id',$finger_id)
                    ->update([
                        'work_io' => '1', 
                        'state' => 'OK', 
                    ]);
                }else
                {
                    $is_exists = AbsensiBBIS::where([
                        [db::raw('trim(nik)'),trim($nik)],
                        [db::raw('trim("action")'),trim($finger_status)],
                        [db::raw("to_char(\"date\",'yyyy-mm-dd')"),$_finger_date],
                    ])
                    ->first();

                    if($finger_status == 'in')
                    {
    
                        if($finger_date < $is_exists->created_time){
                            $_finger_id = $is_exists->finger_id;
                            $is_exists->finger_id = $finger_id;
                            $is_exists->created_time = $finger_date;
                            $is_exists->date = $_finger_date;
                            $is_exists->save();
    
                            DB::connection('finger_bbis')
                            ->table('att_log')
                            ->where('att_id',$finger_id)
                            ->update([
                                'work_io' => '1', 
                                'state' => 'OK', 
                            ]);
                        }
    
                        DB::connection('finger_bbis')
                        ->table('att_log')
                        ->where([
                            ['att_id','!=',$is_exists->finger_id],
                            ['io_mode','0'],
                            ['pin',$nik],
                            [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                        ])
                        ->update([
                            'work_io' => '1', 
                            'state' => 'NOT-OK, DATENGNYA DULUAN ID '.$is_exists->finger_id, 
                        ]);
                        
                    }else if($finger_status == 'out')
                    {
                        if($finger_date > $is_exists->created_time){
                            $_finger_id = $is_exists->finger_id;
                            $is_exists->finger_id = $finger_id;
                            $is_exists->created_time = $finger_date;
                            $is_exists->date = $_finger_date;
                            $is_exists->save();
    
                            DB::connection('finger_bbis')
                            ->table('att_log')
                            ->where('att_id',$finger_id)
                            ->update([
                                'work_io' => '1', 
                                'state' => 'OK', 
                            ]);
                        }
    
                        DB::connection('finger_bbis')
                        ->table('att_log')
                        ->where([
                            ['att_id','!=',$is_exists->finger_id],
                            ['io_mode','1'],
                            ['pin',$nik],
                            [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                        ])
                        ->update([
                            'work_io' => '1', 
                            'state' => 'NOT-OK, PULANGNYA LAMAAN ID '.$is_exists->finger_id, 
                        ]);
                    }
    
    
                }
            }else
            {
                DB::connection('finger_bbis')
                ->table('att_log')
                ->where([
                    ['att_id',$finger_id],
                    ['pin',$nik],
                    [db::raw("DATE_FORMAT(scan_date,'%Y-%m-%d')"),$_finger_date],
                ])
                ->update([
                    'work_io' => '1', 
                    'state' => 'NOT-OK, NIK INI TIDAK TERDAFTAR DI ABSENSI', 
                ]);
            }
            
        }
        return response()->json($data);
       
    }
}
