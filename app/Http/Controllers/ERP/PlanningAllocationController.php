<?php namespace App\Http\Controllers\ERP;

use DB;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;

use App\Models\Erp\PoSupplier;
use App\Models\Erp\PlanningAllocation;

use App\Models\User;

class PlanningAllocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('erp.planning_allocation.index',compact('msg'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $data     = PoSupplier::orderby('period_po','desc');
            //$data     = User::orderby('created_at','desc');
        
            return DataTables::of($data)
            ->addColumn('action',function($data){
                return view('erp.planning_allocation._action', [
                    'model'             => $data,
                    'allocation'        => route('planningAllocation.allocation',$data->c_order_id)
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function allocation(Request $request,$id)
    {
        //$data       = PlanningAllocation::where('c_order_id',$id)->get();
        $data       = DB::connection('erp_live') //dev_erp //erp
        ->table('rma_wms_planning_v2_cron')
        ->where('c_order_id',$id)
        ->get();

        try
        {
            db::beginTransaction();

            $inserted_at = carbon::now();
            foreach ($data as $key => $value) 
            {
                $is_exists = PlanningAllocation::where([
                    ['c_order_id',$value->c_order_id],
                    ['item_id',$value->item_id],
                    ['c_bpartner_id',$value->c_bpartner_id],
                    ['m_warehouse_id',$value->m_warehouse_id],
                    ['po_buyer',$value->po_buyer],
                    ['qty',$value->qty_pr],
                ])
                ->exists();

                if(!$is_exists)
                {
                    PlanningAllocation::FirstOrCreate([
                        'c_order_id'        => $value->c_order_id,
                        'item_id'           => $value->item_id,
                        'c_bpartner_id'     => $value->c_bpartner_id,
                        'supplier_name'     => $value->supplier_name,
                        'item_code'         => $value->item_code,
                        'document_no'       => $value->document_no,
                        'po_buyer'          => $value->po_buyer,
                        'm_warehouse_id'    => $value->m_warehouse_id,
                        'warehouse'         => $value->warehouse,
                        'uom'               => $value->uom_pr,
                        'category'          => $value->category,
                        'qty'               => $value->qty_pr,
                        'isfabric'          => $value->isfabric,
                        'status_lc'         => $value->status_lc,
                        'created'           => $inserted_at,
                        'updated'           => $inserted_at,
                        'isintegrate'       => false,
                        'source'            => 'MIDDLEWARE AOI',
                    ]);
                }
                
            }
            
            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
        }
    }
}
