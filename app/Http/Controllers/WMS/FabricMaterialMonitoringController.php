<?php

namespace App\Http\Controllers\WMS;


use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Wms\AutoAllocations;
use App\Wms\MaterialStock;
// use App\Wms\DashboardErp;
use App\Http\Controllers\Controller;

class FabricMaterialMonitoringController extends Controller
{

    static function insertAllocation()
    {
        $warehouse_id = ['1000011','1000001'];
        $replika_wms = DB::connection('replika_wms');

        // $refresh = DB::select(db::raw("
        // refresh materialized view monitoring_allocations_mv;"
        // ));

        //list kalkulasi
        $auto_allocations = $replika_wms->table('monitoring_allocations_v')
        ->where('is_additional',false)
        ->where('lc_date', '>=', '2020-08-01')
        //->where('lc_date', '<', '2020-08-01')
        // ->where('id', '291f8ce0-f17e-11eb-912f-21cfbed01679')
        ->where('qty_allocated', '>', '0')
        ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>=' ,'0')
        ->whereIn('warehouse_id',$warehouse_id)
        ->orderBy('statistical_date', 'asc')
        ->orderBy('promise_date', 'asc')
        ->orderBy('qty_allocation', 'asc')
        ->get();

        // dd($auto_allocations);

        $delete = AutoAllocations::where('is_fabric', true)
        ->whereIn('warehouse_id',$warehouse_id)->delete();

        try {
            DB::beginTransaction();
            foreach ($auto_allocations as $key => $auto_allocation) {


                $insert_allocations = AutoAllocations::firstOrCreate([
                            // 'auto_allocation_id'     => $auto_allocation->id,
                            // 'type_stock'             => $auto_allocation->type_stock,
                            // 'promise_date'           => $auto_allocation->promise_date,
                            // 'created_at'             => $auto_allocation->created_at,
                            // 'updated_at'             => $auto_allocation->updated_at,
                            // 'lc_date'                => $auto_allocation->lc_date,
                            // 'season'                 => $auto_allocation->season,
                            // 'document_no'            => $auto_allocation->document_no,
                            // 'c_bpartner_id'          => $auto_allocation->c_bpartner_id,
                            // 'supplier_code'          => $auto_allocation->supplier_code,
                            // 'supplier_name'          => $auto_allocation->supplier_name,
                            // 'po_buyer'               => $auto_allocation->po_buyer,
                            // 'old_po_buyer'           => $auto_allocation->po_buyer,
                            // 'item_code_source'       => $auto_allocation->item_code_source,
                            // 'item_code'              => $auto_allocation->item_code,
                            // 'item_desc'              => $auto_allocation->item_desc,
                            // 'category'               => $auto_allocation->category,
                            // 'warehouse_id'           => $auto_allocation->warehouse_id,
                            // 'warehouse_name'         => $auto_allocation->warehouse_name,
                            // 'uom'                    => $auto_allocation->uom,
                            // 'qty_allocation'         => sprintf('%0.4f',$auto_allocation->qty_allocation),
                            // 'qty_outstanding'        => sprintf('%0.4f',$auto_allocation->qty_outstanding),
                            // 'qty_allocated'          => sprintf('%0.4f',$auto_allocation->qty_allocated),
                            // 'qty_in_house'           => sprintf('%0.4f',$auto_allocation->qty_in_house),
                            // 'qty_on_ship'            => sprintf('%0.4f',$auto_allocation->qty_on_ship),
                            // 'is_fabric'              => $auto_allocation->is_fabric,
                            // 'is_additional'          => $auto_allocation->is_additional,
                            // 'remark_additional'      => $auto_allocation->remark_additional,
                            // 'status_po_buyer'        => $auto_allocation->status_po_buyer,
                            // 'cancel_date'            => $auto_allocation->cancel_date,
                            // 'remark_update'          => $auto_allocation->remark_update,
                            // 'is_allocation_purchase' => $auto_allocation->is_allocation_purchase,
                            // 'item_id_source'         => $auto_allocation->item_id_source,
                            // 'c_order_id'             => $auto_allocation->c_order_id,
                            // 'etd_date'               => $auto_allocation->etd_date,
                            // 'eta_date'               => $auto_allocation->eta_date,
                            // 'etd_actual'             => $auto_allocation->etd_actual,
                            // 'eta_actual'             => $auto_allocation->eta_actual,
                            // 'receive_date'           => $auto_allocation->receive_date,
                            // 'no_invoice'             => $auto_allocation->no_invoice,
                            // 'mrd'                    => $auto_allocation->mrd,
                            // 'dd_pi'                  => $auto_allocation->dd_pi,
                            // 'is_closed'              => $auto_allocation->is_closed,
                            // 'eta_delay'              => $auto_allocation->eta_delay,
                            // 'date_closed'            => $auto_allocation->date_closed,
                            // 'eta_max'                => $auto_allocation->eta_max,
                            // 'item_id_book'           => $auto_allocation->item_id_book,
                            // 'status'                 => $auto_allocation->status,
                            // 'brand'                  => $auto_allocation->brand,
                            // '_style'                 => $auto_allocation->_style,
                            // 'remark_delay'           => $auto_allocation->remark_delay,
                            // 'supplier_code_bom'      => $auto_allocation->supplier_code_bom,
                            // 'supplier_name_bom'      => $auto_allocation->supplier_name_bom,
                            // 'qty_ordered_garment'    =>sprintf('%0.4f',$auto_allocation->qty_ordered_garment),
                            // 'podd'                   =>$auto_allocation->podd,
                            // 'is_already_generate_form_booking' => $auto_allocation->is_already_generate_form_booking,
                            // 'generate_form_booking' => $auto_allocation->generate_form_booking,
                            // 'is_inserted_to_allocation_item' => $auto_allocation->is_inserted_to_allocation_item,
                            // 'deleted_at'            => $auto_allocation->deleted_at,
                            // 'remark'                => $auto_allocation->remark,
                            // 'document_allocation_number'=> $auto_allocation->document_allocation_number,
                            // 'is_upload_manual'  => $auto_allocation->is_upload_manual,
                            // 'user_id'           => $auto_allocation->user_id,
                            // 'update_user_id'    => $auto_allocation->update_user_id,
                            // 'qc_note'           => $auto_allocation->qc_note,
                            // 'qty_reject'        => $auto_allocation->qty_reject,
                            // 'type_stock_erp_code' => $auto_allocation->type_stock_erp_code,
                            // 'item_code_book'    => $auto_allocation->item_code_book,
                            // 'article_no'        => $auto_allocation->article_no,
                            // 'po_buyer_source'   => $auto_allocation->po_buyer_source,
                            // 'is_from_menu_allocation_buyer' => $auto_allocation->is_from_menu_allocation_buyer,
                            // 'qty_adjustment'    => $auto_allocation->qty_adjustment,
                            // 'erp_allocation_id' => $auto_allocation->erp_allocation_id,
                            // 'is_reduce'         => $auto_allocation->is_reduce,
                            // 'is_ordering_for_japan_china'   => $auto_allocation->is_ordering_for_japan_china,
                            // 'locked_date'   => $auto_allocation->locked_date,
                            // 'is_reroute'    => $auto_allocation->is_reroute,
                            // 'psd'   => $auto_allocation->psd,
                            // 'qty_mrd'   => $auto_allocation->qty_mrd,
                            // 'is_remove_from_dashboard'=> $auto_allocation->is_remove_from_dashboard,
                            // 'remark_remove' => $auto_allocation->remark_remove,
                            // 'user_id_remove'  => $auto_allocation->user_id_remove,
                            // 'is_balance_marker'=> $auto_allocation->is_balance_marker,
                            // 'is_request_delete' => $auto_allocation->is_request_delete,
                            // 'user_pic'      => $auto_allocation->user_pic,
                            // 'update_user' => $auto_allocation->update_user,
                            // 'date_closed'   => $auto_allocation->date_closed,
                            // 'eta_max'       => $auto_allocation->eta_max,
                            // '_style'        => $auto_allocation->_style,
                            'auto_allocation_id'     => $auto_allocation->id,
                            'type_stock'             => $auto_allocation->type_stock,
                            'promise_date'           => $auto_allocation->promise_date,
                            'booking_number'         => $auto_allocation->booking_number,
                            'created_at'             => $auto_allocation->created_at,
                            'user_pic'               => $auto_allocation->user_pic,
                            'update_user'            => $auto_allocation->update_user,
                            'lc_date'                => $auto_allocation->lc_date,
                            'season'                 => $auto_allocation->season,
                            'document_no'            => $auto_allocation->document_no,
                            'supplier_code'          => $auto_allocation->supplier_code,
                            'supplier_name'          => $auto_allocation->supplier_name,
                            'po_buyer'               => $auto_allocation->po_buyer,
                            'old_po_buyer'           => $auto_allocation->po_buyer,
                            'item_code_source'       => $auto_allocation->item_code_source,
                            'item_code'              => $auto_allocation->item_code,
                            'item_desc'              => $auto_allocation->item_desc,
                            'category'               => $auto_allocation->category,
                            'warehouse_id'           => $auto_allocation->warehouse_id,
                            'warehouse_name'         => $auto_allocation->warehouse_name,
                            'uom'                    => $auto_allocation->uom,
                            'qty_allocation'         => sprintf('%0.4f',$auto_allocation->qty_allocation),
                            'qty_outstanding'        => sprintf('%0.4f',$auto_allocation->qty_outstanding),
                            'qty_allocated'          => sprintf('%0.4f',$auto_allocation->qty_allocated),
                            'qty_in_house'           => sprintf('%0.4f',$auto_allocation->qty_in_house),
                            'qty_on_ship'            => sprintf('%0.4f',$auto_allocation->qty_on_ship),
                            'is_fabric'              => $auto_allocation->is_fabric,
                            'is_additional'          => $auto_allocation->is_additional,
                            'remark_additional'      => $auto_allocation->remark_additional,
                            'status_po_buyer'        => $auto_allocation->status_po_buyer,
                            'cancel_date'            => $auto_allocation->cancel_date,
                            'remark_update'          => $auto_allocation->remark_update,
                            'is_allocation_purchase' => $auto_allocation->is_allocation_purchase,
                            'item_id_source'         => $auto_allocation->item_id_source,
                            'c_order_id'             => $auto_allocation->c_order_id,
                            'etd_date'               => $auto_allocation->etd_date,
                            'eta_date'               => $auto_allocation->eta_date,
                            'etd_actual'             => $auto_allocation->etd_actual,
                            'eta_actual'             => $auto_allocation->eta_actual,
                            'receive_date'           => $auto_allocation->receive_date,
                            'no_invoice'             => $auto_allocation->no_invoice,
                            'mrd'                    => $auto_allocation->mrd,
                            'dd_pi'                  => $auto_allocation->dd_pi,
                            'is_closed'              => $auto_allocation->is_closed,
                            'eta_delay'              => $auto_allocation->eta_delay,
                            'date_closed'            => $auto_allocation->date_closed,
                            'eta_max'                => $auto_allocation->eta_max,
                            'item_id_book'           => $auto_allocation->item_id_book,
                            'status'                 => $auto_allocation->status,
                            'brand'                  => $auto_allocation->brand,
                            '_style'                 => $auto_allocation->_style,
                            'remark_delay'           => $auto_allocation->remark_delay,
                            'supplier_code_bom'      => $auto_allocation->supplier_code_bom,
                            'supplier_name_bom'      => $auto_allocation->supplier_name_bom,
                            'qty_ordered_garment'    =>sprintf('%0.4f',$auto_allocation->qty_ordered_garment),
                            'podd'                   =>$auto_allocation->podd,
                            'statistical_date'       => $auto_allocation->statistical_date


                ]);

                
        
             }
            DB::commit();
            // dd($insert_allocations);
            
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

       
    }

    static function updateMonitoringMaterial()
    {
        $warehouse_id = ['1000011','1000001'];
        $replika_wms = DB::connection('replika_wms');
        $change_date = carbon::now()->format('Y-m-d');

        // $app_env = Config::get('app.env');
        // if($app_env == 'live')
        // {
            $data_erp = DB::connection('erp_live');
            $data     = DB::connection('web_po');
        // }

        // $auto_allocations = $replika_wms->table('monitoring_allocations_mv')
        // ->where('is_additional',false)
        // ->where('lc_date', '>=', '2020-08-01')
        // //->where('lc_date', '<', '2020-08-01')
        // //->where('document_no', 'POA22007A/E0396')
        // ->where('qty_allocated', '>', '0')
        // ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
        // ->whereIn('warehouse_id',$warehouse_id)
        // ->orderBy('statistical_date', 'asc')
        // ->orderBy('promise_date', 'asc')
        // ->orderBy('qty_allocation', 'asc')
        // ->get();
        $auto_allocations = AutoAllocations::whereIn('warehouse_id',$warehouse_id)
        ->where('is_fabric', true)
        ->orderBy('statistical_date', 'asc')
        ->orderBy('promise_date', 'asc')
        ->orderBy('qty_allocation', 'asc')
        ->get();
        

        //dd($auto_allocations);
        try 
            {
            DB::beginTransaction();

            $reset_alocation = AutoAllocations::where('is_additional',false)
            //->whereBetween('lc_date', [$start_date, $end_date])
            ->where('lc_date', '>=', '2020-06-01')
            //->where(db::raw("to_char(lc_date,'YYYYMM')"),['202006'])
            //->where('document_no', 'POA22007A/E0396')
            ->where('qty_allocated', '>', '0')
            ->where(db::raw("qty_allocated - COALESCE(qty_in_house, 0)"), '>' ,'0')
            ->whereIn('warehouse_id',$warehouse_id)
            ->whereNull('deleted_at')
            ->update([
                'qty_in_house' => null,
                'qty_on_ship'  => null,
                'qty_mrd'      => null,
                // 'eta_actual'   => null,
                // 'eta_actual'   => null,
                // 'no_invoice'   => null,
                // 'receive_date' => null,
                // 'mrd'          => null,
                // 'dd_pi'        => null,
                // 'eta_date'     => null,
                // 'etd_date'     => null,
            ]);

            foreach($auto_allocations as $key => $auto_allocation)
            {   
                $eta_date     = null;
                $etd_date     = null;
                $receive_date = null;
                $no_invoice   = null;
                $qty_in_house = 0;
                $qty_on_ship  = 0;
                $qty_mrd      = 0;
                $mrd          = null;
                $dd_pi        = null;
                $eta_pi       = null;
                $etd_pi       = null;

                $auto_allocation_id = $auto_allocation->auto_allocation_id;
                $item_id            = $auto_allocation->item_id_source;
                $warehouse_id       = $auto_allocation->warehouse_id;
                $c_order_id         = $auto_allocation->c_order_id;
                $lc_date             = $auto_allocation->lc_date;
                $qty_need           = sprintf('%0.4f',$auto_allocation->qty_allocation);
                $qty_on_ship_alocation = sprintf('%0.4f',$auto_allocation->qty_on_ship);

                if($c_order_id == 'FREE STOCK')
                {
                    $qty_in_house = $qty_need;
                    $eta_actual   = $lc_date;
                    $etd_actual   = $lc_date;
                    $receive_date = $lc_date;
                    $no_invoice   = null;

                }
                else
                {

                $material_stock = MaterialStock::select(db::raw('sum(stock) as stock'), db::raw('max(created_at) as created_at'))
                ->where([
                ['item_id', $item_id],
                ['c_order_id', $c_order_id],
                ['is_stock_mm', false],
                ['is_closing_balance', false],
                ['approval_date', '!=', null],
                ['is_from_handover', false],
                ['is_master_roll', true],
                //['warehouse_id', $warehouse_id], tidak cek warehouse
                ])
                ->whereNull('case')
                ->first();
                //->whereNull('deleted_at')
                //->sum('stock');
                //dd($material_stock);
                $sum_qty_stock = $material_stock->stock;
                $receive_date    = $material_stock->created_at;
                //$no_invoice    = $material_stock->no_invoice;

                $sum_qty_in_house = AutoAllocations::where([
                    ['item_id_source', $item_id],
                    ['c_order_id', $c_order_id],
                    //['warehouse_id', $warehouse_id],
                    ])
                    ->whereNull('deleted_at')
                    ->sum('qty_in_house');

                //sisa qty in house 
                $material_in_house = sprintf('%0.4f',$sum_qty_stock) - sprintf('%0.4f',$sum_qty_in_house);
                
                //jika material yang tersdia > 0
                if($material_in_house > 0)
                {
                    //dd($material_in_house - $qty_need);
                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                    //dd($material_in_house/$qty_need);
                    if(sprintf('%0.4f',$material_in_house)/$qty_need >= 1)
                    {
                        $qty_in_house = $qty_need;
                        $qty_on_ship  = 0;

                        //$app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                            //$data = DB::connection('web_po');
                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],//ganti dulu 
                                ['flag_wms', true]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            //dd($list_on_ships);

                            if(count($list_on_ships) > 0)
                            {
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocations::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocations::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                    
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0 )
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                //$receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocations::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //$receive_date = $receive_date;
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                        //dd($no_invoice);

                        //$data_erp = DB::connection('erp');

                        // $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        // ->select(db::raw('max(mrd) as mrd, max(dd_pi) as dd_pi, max(eta) as eta, max(etd) as etd'))
                        // ->where([
                        //     ['m_product_id', $item_id],
                        //     ['c_order_id', $c_order_id],
                        // ])
                        // ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        // ->orderBy('eta', 'desc')
                        // ->orderBy('etd', 'desc')
                        // ->first();

                        // //dd($list_on_ship_mrds);
                        // if($list_on_ship_mrds)
                        // {
                        //     $mrd          = $list_on_ship_mrds->mrd;
                        //     $dd_pi        = $list_on_ship_mrds->dd_pi;
                        //     $eta_pi       = $list_on_ship_mrds->eta;
                        //     $etd_pi       = $list_on_ship_mrds->etd;
                        // }

                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocations::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }

                    }
                    else
                    {
                        $qty_in_house = $material_in_house;
                        //cek kekurangan di web po
                        // $app_env = Config::get('app.env');
                        // if($app_env == 'live')
                        // {
                        //     $data = DB::connection('web_po');
                        $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                        ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                        ->where([
                            ['m_product_id', $item_id],
                            ['c_order_id', $c_order_id],
                        ])
                        ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                        ->orderBy('mrd', 'asc')
                        ->orderBy('dd_pi', 'asc')
                        ->get();
                        
                        //dd($list_on_ship_mrds);
                        $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                        if(count($list_on_ship_mrds) > 0)
                        {
                            $temp_qty_on_ship_mrd = 0;
                            foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                            {
                                $sisa = 0;
                                $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                $sum_all_qty_on_ship_mrd = AutoAllocations::where([
                                    ['item_id_source', $item_id],
                                    ['c_order_id', $c_order_id],
                                    ['warehouse_id', $warehouse_id],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->sum('qty_mrd');
                                
                                    
                                if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                {
                                    continue;
                                }
                                else
                                {
                                    $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                }

                                if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                {
                                    if($outstanding_on_ship_mrd > 0)
                                    {
                                        //jika material yang tersedia lebih dari atau sama dengan alokasi
                                        if($outstanding_on_ship_mrd >= $qty_need)
                                        {
                                            $qty_mrd      = $qty_need;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            //$receive_date = null;
                                            //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                            break;
                                        }
                                        else
                                        {
                                            $qty_mrd       += $outstanding_on_ship_mrd;
                                            $mrd          = $list_on_ship_mrd->mrd;
                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                            $eta_pi       = $list_on_ship_mrd->eta;
                                            $etd_pi       = $list_on_ship_mrd->etd;
                                            $receive_date = $receive_date;
                                            //echo $qty_mrd. PHP_EOL;

                                            if($qty_mrd >= $qty_need)
                                            {
                                                $qty_mrd      = $qty_need;
                                                $mrd          = $list_on_ship_mrd->mrd;
                                                $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                $eta_pi       = $list_on_ship_mrd->eta;
                                                $etd_pi       = $list_on_ship_mrd->etd;
                                                //$receive_date = null;
                                                //echo $qty_mrd. PHP_EOL;
                                                break;
                                                
                                            }
                                        }
                                        

                                    }
                                }
                            }
                        }

                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],
                                ['flag_wms', false]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            //dd($list_on_ships);
                            $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            if(count($list_on_ships) > 0)
                            {
                                $temp_qty_on_ship = 0;
                                $outstanding_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocations::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocations::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
        
                                    // $qty_on_ship_web_po += $sisa_qty_on_ship_web_po;
                                    
                                    //sisa qty on ship
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    $receive_date = $receive_date;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                            }
                            else
                            {
                                // $qty_on_ship  = 0;
                                // $eta_date     = null;
                                // $etd_date     = null;
                                // $receive_date = null;
                                // $no_invoice   = null;
                                //dd('sini');
                                //get MRD

                                // //cek web po yang sudah diterima 
                                // $app_env = Config::get('app.env');
                                // if($app_env == 'live')
                                // {
                                //     $data = DB::connection('web_po');
                                //     $list_eta_etd_in_house = $data->table('material_on_ship_v')
                                //     ->select(db::raw('max(kst_etadate) as etadate , max(kst_etddate) as etddate, kst_invoicevendor'))
                                //     ->where([
                                //         ['m_product_id', $item_id],
                                //         ['c_order_id', $c_order_id],
                                //         ['isactive', true],
                                //         ['flag_wms', true]
                                //     ])
                                //     ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                                //     ->orderBy('kst_etadate', 'asc')
                                //     ->orderBy('kst_etddate', 'asc')
                                //     ->first();
                                // }
                                //dd($list_eta_etd_in_house);
                                // if($list_eta_etd_in_house)
                                // {
                                //     $eta_date     = $list_eta_etd_in_house->etadate;
                                //     $etd_date     = $list_eta_etd_in_house->etddate;
                                //     $receive_date = $receive_date;
                                //     $no_invoice   = $list_eta_etd_in_house->kst_invoicevendor;
                                // }

                                // if($app_env == 'live')
                                // {
                                    //$data_erp = DB::connection('erp');

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocations::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                //}
                            }

                        //}

                    }

                }
                else
                {
                    $qty_in_house = 0;
                    //cek qty on ship di web po 
                    // $app_env = Config::get('app.env');
                    // if($app_env == 'live')
                    //     {
                            //$data = DB::connection('web_po');

                            $list_on_ships = $data->table('material_on_ship_v')
                            ->select(db::raw('sum(qty_upload) as qty_upload'), 'kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->where([
                                ['m_product_id', $item_id],
                                ['c_order_id', $c_order_id],
                                ['isactive', true],//ganti dulu 
                                ['flag_wms', false]
                            ])
                            ->groupBy('kst_etadate', 'kst_etddate', 'kst_invoicevendor')
                            ->orderBy('kst_etadate', 'asc')
                            ->orderBy('kst_etddate', 'asc')
                            ->get();

                            $sum_on_ship_web_po = $list_on_ships->sum('qty_upload');

                            //dd($list_on_ships);

                            if(count($list_on_ships) > 0)
                            {
                                $temp_qty_on_ship = 0;
                                foreach($list_on_ships as $key => $list_on_ship)
                                {
                                    $sisa = 0;
                                    $no_invoice           = $list_on_ship->kst_invoicevendor;
                                    $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship->qty_upload);
                                    $temp_qty_on_ship += $total_on_ship_web_po;

                                    $sum_all_qty_on_ship = AutoAllocations::where([
                                        ['item_id_source', $item_id],
                                        ['c_order_id', $c_order_id],
                                        //['warehouse_id', $warehouse_id],
                                        ])
                                        ->whereNull('deleted_at')
                                        ->sum('qty_on_ship');

                                    // $sum_qty_on_ship = AutoAllocations::where([
                                    //     ['item_id_source', $item_id],
                                    //     ['c_order_id', $c_order_id],
                                    //     ['warehouse_id', $warehouse_id],
                                    //     ['no_invoice', $no_invoice],
                                    //     ])
                                    //     ->whereNull('deleted_at')
                                    //     ->sum('qty_on_ship');
                                    
                                       
                                    if(sprintf('%0.4f', $sum_all_qty_on_ship) - $temp_qty_on_ship > 0)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        $outstanding_on_ship =  $temp_qty_on_ship - sprintf('%0.4f', $sum_all_qty_on_ship);
                                    }

                                    //$outstanding_on_ship = sprintf('%0.4f',$total_on_ship_web_po) - sprintf('%0.4f',$sum_qty_on_ship);
                                    
                                    //echo $no_invoice. ' total  '. $total_on_ship_web_po. ' sisa '.$outstanding_on_ship. PHP_EOL;

                                    //echo (sprintf('%0.4f',$sum_on_ship_web_po) - sprintf('%0.4f', $sum_all_qty_on_ship)). PHP_EOL;
                                    if(sprintf('%0.4f',$sum_on_ship_web_po) -sprintf('%0.4f', $sum_all_qty_on_ship) != 0)
                                    {
                                        if($outstanding_on_ship > 0)
                                        {
                                            //jika material yang tersedia lebih dari atau sama dengan alokasi
                                            if($outstanding_on_ship >= ($qty_need - $qty_in_house))
                                            {
                                                $qty_on_ship              = $qty_need - $qty_in_house;
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = null;
                                                //echo $qty_on_ship. PHP_EOL;
                                                break;
                                            }
                                            else
                                            {
                                                $qty_on_ship       += $outstanding_on_ship;
                                                if($qty_on_ship >= ($qty_need - $qty_in_house))
                                                {
                                                    $qty_on_ship  = $qty_need - $qty_in_house;
                                                    $eta_date     = $list_on_ship->kst_etadate;
                                                    $etd_date     = $list_on_ship->kst_etddate;
                                                    //$receive_date = null;
                                                    //echo $qty_on_ship. PHP_EOL;
                                                    break;
                                                    
                                                }
                                                $eta_date                 = $list_on_ship->kst_etadate;
                                                $etd_date                 = $list_on_ship->kst_etddate;
                                                $receive_date             = $receive_date;
                                                //echo $qty_on_ship. PHP_EOL;
                                            }
                                            

                                        }
                                    }
                                    // else
                                    // {
                                    //     $qty_on_ship  = 0;
                                    //     $eta_date     = null;
                                    //     $etd_date     = null;
                                    //     $receive_date = null;
                                    // }    
                                }
                                //get mrd
                                $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();
                                    
                                    //dd($list_on_ship_mrds);
                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_qty_mrd = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_qty_mrd;

                                            $sum_all_qty_on_ship_mrd = AutoAllocations::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        //$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_mrd. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                        //echo $qty_mrd. PHP_EOL;
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                // $qty_on_ship  = 0;
                                // $eta_date     = null;
                                // $etd_date     = null;
                                // $receive_date = null;
                                // $no_invoice   = null;

                                //get MRD
                                // if($app_env == 'live')
                                // {
                                    //$data_erp = DB::connection('erp');

                                    $list_on_ship_mrds = $data_erp->table('rma_po_mrd')
                                    ->select(db::raw('sum(qtyordered) as qtyordered'), 'mrd', 'dd_pi', 'eta', 'etd')
                                    ->where([
                                        ['m_product_id', $item_id],
                                        ['c_order_id', $c_order_id],
                                    ])
                                    ->groupBy('mrd', 'dd_pi', 'eta', 'etd')
                                    ->orderBy('mrd', 'asc')
                                    ->orderBy('dd_pi', 'asc')
                                    ->get();

                                    $sum_on_ship_mrds = $list_on_ship_mrds->sum('qtyordered');

                                    if(count($list_on_ship_mrds) > 0)
                                    {
                                        $temp_qty_on_ship_mrd = 0;
                                        foreach($list_on_ship_mrds as $key => $list_on_ship_mrd)
                                        {
                                            $sisa = 0;
                                            $total_on_ship_web_po = sprintf('%0.4f',$list_on_ship_mrd->qtyordered);
                                            $temp_qty_on_ship_mrd += $total_on_ship_web_po;

                                            $sum_all_qty_on_ship_mrd = AutoAllocations::where([
                                                ['item_id_source', $item_id],
                                                ['c_order_id', $c_order_id],
                                                //['warehouse_id', $warehouse_id],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->sum('qty_mrd');
                                            
                                                
                                            if(sprintf('%0.4f', $sum_all_qty_on_ship_mrd) - $temp_qty_on_ship_mrd > 0)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                $outstanding_on_ship_mrd =  $temp_qty_on_ship_mrd - sprintf('%0.4f', $sum_all_qty_on_ship_mrd);
                                            }

                                            if(sprintf('%0.4f',$sum_on_ship_mrds) -sprintf('%0.4f', $sum_all_qty_on_ship_mrd) != 0)
                                            {
                                                if($outstanding_on_ship_mrd > 0)
                                                {
                                                    //jika material yang tersedia lebih dari atau sama dengan alokasi
                                                    if($outstanding_on_ship_mrd >= $qty_need)
                                                    {
                                                        $qty_mrd      = $qty_need;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        ////$receive_date = null;
                                                        //echo $qty_mrd. $list_on_ship_mrd->mrd. PHP_EOL;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        $qty_mrd       += $outstanding_on_ship_mrd;
                                                        $mrd          = $list_on_ship_mrd->mrd;
                                                        $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                        $eta_pi       = $list_on_ship_mrd->eta;
                                                        $etd_pi       = $list_on_ship_mrd->etd;
                                                        $receive_date = $receive_date;
                                                        //echo $qty_on_ship. PHP_EOL;

                                                        if($qty_mrd >= $qty_need)
                                                        {
                                                            $qty_mrd      = $qty_need;
                                                            $mrd          = $list_on_ship_mrd->mrd;
                                                            $dd_pi        = $list_on_ship_mrd->dd_pi;
                                                            $eta_pi       = $list_on_ship_mrd->eta;
                                                            $etd_pi       = $list_on_ship_mrd->etd;
                                                            //$receive_date = null;
                                                            //echo $qty_on_ship. PHP_EOL;
                                                            break;
                                                            
                                                        }
                                                    }
                                                    

                                                }
                                            }
                                        }
                                    }
                                //}
                            }

                    //}
                }

            }
                //kalau parsial masih ada qty open
                if(sprintf('%0.4f', $qty_need - $qty_in_house - $qty_on_ship) > 0.001)
                {
                    $eta_date     = null;
                    $etd_date     = null;
                    $receive_date = null;
                    $no_invoice   = null;
                }
                
                $auto_allocation_update               = AutoAllocations::find($auto_allocation_id);
                $auto_allocation_update->qty_in_house = $qty_in_house;
                $auto_allocation_update->qty_on_ship  = $qty_on_ship;
                if($auto_allocation_update->eta_actual != null)
                {
                    if($auto_allocation_update->eta_actual != $eta_date)
                    {
                        $auto_allocation_update->remark_delay = $auto_allocation_update->remark_delay.' Delay Pengiriman '.$change_date;
                    }
                }
                $auto_allocation_update->eta_actual   = $eta_date;
                $auto_allocation_update->etd_actual   = $etd_date;
                $auto_allocation_update->eta_date     = $eta_pi;
                $auto_allocation_update->etd_date     = $etd_pi;
                $auto_allocation_update->mrd          = $mrd;
                $auto_allocation_update->dd_pi        = $dd_pi;
                $auto_allocation_update->qty_mrd      = $qty_mrd;
                $auto_allocation_update->receive_date = $receive_date;
                $auto_allocation_update->no_invoice   = $no_invoice;
                $auto_allocation_update->save();

            }

                DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                }


    }

    static function insertToDashboardErp()
    {
        $dashboard_wms = DB::connection('dashboard_wms');
        $warehouse_id = ['1000011','1000001'];
        $auto_allocations = $dashboard_wms->table('monitoring_allocation')
                ->where('is_additional',false)
                ->where('lc_date', '>=', '2020-08-01')
                ->where('is_fabric', true)
                ->whereIn('warehouse_id',$warehouse_id)
                ->orderBy('statistical_date', 'asc')
                ->orderBy('promise_date', 'asc')
                ->orderBy('qty_allocation', 'asc')
                ->get();

        //insertToErp

        // $app_env = Config::get('app.env');
        // if($app_env == 'live')
        // {
            // $data_erp = DB::connection('erp_live');
        // }

        //kosongin dulu tabelnya 
        $delete = DashboardErp::truncate();
        //isiin lagi pakai data terbaru
        foreach($auto_allocations as $key => $auto_allocation)
        {
            $dashboard = DashboardErp::insert([
                        'id'                     => $auto_allocation->id,
                        'type_stock'             => $auto_allocation->type_stock,
                        'promise_date'           => $auto_allocation->promise_date,
                        'statistical_date'       => $auto_allocation->podd,
                        'booking_number'         => $auto_allocation->booking_number,
                        'created_at'             => $auto_allocation->created_at,
                        'user_pic'               => $auto_allocation->user_pic,
                        'update_user'            => $auto_allocation->update_user,
                        'lc_date'                => $auto_allocation->lc_date,
                        'season'                 => $auto_allocation->season,
                        'document_no'            => $auto_allocation->document_no,
                        'supplier_code'          => $auto_allocation->supplier_code,
                        'supplier_name'          => $auto_allocation->supplier_name,
                        'po_buyer'               => $auto_allocation->po_buyer,
                        'old_po_buyer'           => $auto_allocation->old_po_buyer,
                        'item_code_source'       => $auto_allocation->item_code_source,
                        'item_code'              => $auto_allocation->item_code,
                        'item_desc'              => $auto_allocation->item_desc,
                        'category'               => $auto_allocation->category,
                        'warehouse_id'           => $auto_allocation->warehouse_id,
                        'warehouse_name'         => $auto_allocation->warehouse_name,
                        'uom'                    => $auto_allocation->uom,
                        'qty_allocation'         => $auto_allocation->qty_allocation,
                        'qty_outstanding'        => $auto_allocation->qty_outstanding,
                        'qty_allocated'          => $auto_allocation->qty_allocated,
                        'qty_in_house'           => $auto_allocation->qty_in_house,
                        'qty_on_ship'            => $auto_allocation->qty_on_ship,
                        'is_fabric'              => $auto_allocation->is_fabric,
                        'is_additional'          => $auto_allocation->is_additional,
                        'remark_additional'      => $auto_allocation->remark_additional,
                        'status_po_buyer'        => $auto_allocation->status_po_buyer,
                        'cancel_date'            => $auto_allocation->cancel_date,
                        'remark_update'          => $auto_allocation->remark_update,
                        'is_allocation_purchase' => $auto_allocation->is_allocation_purchase,
                        'item_id_source'         => $auto_allocation->item_id_source,
                        'c_order_id'             => $auto_allocation->c_order_id,
                        'eta_date'               => $auto_allocation->eta_date,
                        'etd_date'               => $auto_allocation->etd_date,
                        'eta_actual'             => $auto_allocation->eta_actual,
                        'etd_actual'             => $auto_allocation->etd_actual,
                        'receive_date'           => $auto_allocation->receive_date,
                        'no_invoice'             => $auto_allocation->no_invoice,
                        'mrd'                    => $auto_allocation->mrd,
                        'dd_pi'                  => $auto_allocation->dd_pi,
                        'is_closed'              => $auto_allocation->is_closed,
                        'eta_delay'              => $auto_allocation->eta_delay,
                        'eta_max'                => $auto_allocation->eta_max,
                        'item_id_book'           => $auto_allocation->item_id_book,
                        'status'                 => $auto_allocation->status,
                        'brand'                  => $auto_allocation->brand,
                        '_style'                 => $auto_allocation->_style,
                        'remark_delay_new'       => $auto_allocation->remark_delay,
                        'supplier_code_bom'      => $auto_allocation->supplier_code_bom,
                        'supplier_name_bom'      => $auto_allocation->supplier_name_bom,
                        'qtyordered_garment'      => $auto_allocation->qty_ordered_garment,
                ]);
        }






    }

}
